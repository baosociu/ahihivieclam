/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     10/5/2017 3:08:05 PM                         */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_HOSO_NHA_HOSO_NHAT_NHA_TUYE') then
    alter table HOSO_NHATUYENDUNG
       delete foreign key FK_HOSO_NHA_HOSO_NHAT_NHA_TUYE
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_HOSO_NHA_HOSO_NHAT_HO_SO') then
    alter table HOSO_NHATUYENDUNG
       delete foreign key FK_HOSO_NHA_HOSO_NHAT_HO_SO
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_HO_SO__CO_NGUOI_TI') then
    alter table HO_SO
       delete foreign key FK_HO_SO__CO_NGUOI_TI
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_NGUOI_DU_NGUOI_DUN_NGUOI_DU') then
    alter table NGUOI_DUNG__NHOM_NGUOI_DUNG
       delete foreign key FK_NGUOI_DU_NGUOI_DUN_NGUOI_DU
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_NGUOI_DU_NGUOI_DUN_NHOM_NGU') then
    alter table NGUOI_DUNG__NHOM_NGUOI_DUNG
       delete foreign key FK_NGUOI_DU_NGUOI_DUN_NHOM_NGU
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_NGUOI_TI___CO_NGUOI_DU') then
    alter table NGUOI_TIM_VIEC
       delete foreign key FK_NGUOI_TI___CO_NGUOI_DU
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_NHA_TUYE_CO___NGUOI_DU') then
    alter table NHA_TUYEN_DUNG
       delete foreign key FK_NHA_TUYE_CO___NGUOI_DU
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_NHOMNGUO_NHOMNGUOI_THAO_TAC') then
    alter table NHOMNGUOIDUNG_THAOTAC
       delete foreign key FK_NHOMNGUO_NHOMNGUOI_THAO_TAC
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_NHOMNGUO_NHOMNGUOI_NHOM_NGU') then
    alter table NHOMNGUOIDUNG_THAOTAC
       delete foreign key FK_NHOMNGUO_NHOMNGUOI_NHOM_NGU
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_NHOM_NGU_NHOM_NGUO_MAN_HINH') then
    alter table NHOM_NGUOI_DUNG___MAN_HINH
       delete foreign key FK_NHOM_NGU_NHOM_NGUO_MAN_HINH
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_NHOM_NGU_NHOM_NGUO_NHOM_NGU') then
    alter table NHOM_NGUOI_DUNG___MAN_HINH
       delete foreign key FK_NHOM_NGU_NHOM_NGUO_NHOM_NGU
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_TIN_TUYE_CO_NHA_TUYE') then
    alter table TIN_TUYEN_DUNG
       delete foreign key FK_TIN_TUYE_CO_NHA_TUYE
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_TIN_TUYE_THUOC_TINH_THA') then
    alter table TIN_TUYEN_DUNG
       delete foreign key FK_TIN_TUYE_THUOC_TINH_THA
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_TIN_TUYE_TIN_TUYEN_CHU__E') then
    alter table TIN_TUYEN_DUNG__CHU_DE
       delete foreign key FK_TIN_TUYE_TIN_TUYEN_CHU__E
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_TIN_TUYE_TIN_TUYEN_TIN_TUYE') then
    alter table TIN_TUYEN_DUNG__CHU_DE
       delete foreign key FK_TIN_TUYE_TIN_TUYEN_TIN_TUYE
end if;

drop index if exists CHU__E.CHU__E_PK;

drop table if exists CHU__E;

drop index if exists HOSO_NHATUYENDUNG.HOSO_NHATUYENDUNG2_FK;

drop index if exists HOSO_NHATUYENDUNG.HOSO_NHATUYENDUNG_FK;

drop index if exists HOSO_NHATUYENDUNG.HOSO_NHATUYENDUNG_PK;

drop table if exists HOSO_NHATUYENDUNG;

drop index if exists HO_SO._CO_FK;

drop index if exists HO_SO.HO_SO_PK;

drop table if exists HO_SO;

drop index if exists MAN_HINH.MAN_HINH_PK;

drop table if exists MAN_HINH;

drop index if exists NGUOI_DUNG.NGUOI_DUNG_PK;

drop table if exists NGUOI_DUNG;

drop index if exists NGUOI_DUNG__NHOM_NGUOI_DUNG.NGUOI_DUNG__NHOM_NGUOI_DUNG2_FK;

drop index if exists NGUOI_DUNG__NHOM_NGUOI_DUNG.NGUOI_DUNG__NHOM_NGUOI_DUNG_FK;

drop index if exists NGUOI_DUNG__NHOM_NGUOI_DUNG.NGUOI_DUNG__NHOM_NGUOI_DUNG_PK;

drop table if exists NGUOI_DUNG__NHOM_NGUOI_DUNG;

drop index if exists NGUOI_TIM_VIEC.__CO_FK;

drop index if exists NGUOI_TIM_VIEC.NGUOI_TIM_VIEC_PK;

drop table if exists NGUOI_TIM_VIEC;

drop index if exists NHA_TUYEN_DUNG.CO___FK;

drop index if exists NHA_TUYEN_DUNG.NHA_TUYEN_DUNG_PK;

drop table if exists NHA_TUYEN_DUNG;

drop index if exists NHOMNGUOIDUNG_THAOTAC.NHOMNGUOIDUNG_THAOTAC2_FK;

drop index if exists NHOMNGUOIDUNG_THAOTAC.NHOMNGUOIDUNG_THAOTAC_FK;

drop index if exists NHOMNGUOIDUNG_THAOTAC.NHOMNGUOIDUNG_THAOTAC_PK;

drop table if exists NHOMNGUOIDUNG_THAOTAC;

drop index if exists NHOM_NGUOI_DUNG.NHOM_NGUOI_DUNG_PK;

drop table if exists NHOM_NGUOI_DUNG;

drop index if exists NHOM_NGUOI_DUNG___MAN_HINH.NHOM_NGUOI_DUNG___MAN_HINH2_FK;

drop index if exists NHOM_NGUOI_DUNG___MAN_HINH.NHOM_NGUOI_DUNG___MAN_HINH_FK;

drop index if exists NHOM_NGUOI_DUNG___MAN_HINH.NHOM_NGUOI_DUNG___MAN_HINH_PK;

drop table if exists NHOM_NGUOI_DUNG___MAN_HINH;

drop index if exists THAO_TAC.THAO_TAC_PK;

drop table if exists THAO_TAC;

drop index if exists TINH_THANH.TINH_THANH_PK;

drop table if exists TINH_THANH;

drop index if exists TIN_TUYEN_DUNG.THUOC_FK;

drop index if exists TIN_TUYEN_DUNG.CO_FK;

drop index if exists TIN_TUYEN_DUNG.TIN_TUYEN_DUNG_PK;

drop table if exists TIN_TUYEN_DUNG;

drop index if exists TIN_TUYEN_DUNG__CHU_DE.TIN_TUYEN_DUNG__CHU_DE2_FK;

drop index if exists TIN_TUYEN_DUNG__CHU_DE.TIN_TUYEN_DUNG__CHU_DE_FK;

drop index if exists TIN_TUYEN_DUNG__CHU_DE.TIN_TUYEN_DUNG__CHU_DE_PK;

drop table if exists TIN_TUYEN_DUNG__CHU_DE;

/*==============================================================*/
/* Table: CHU__E                                                */
/*==============================================================*/
create table CHU__E 
(
   MACHUDE              char(6)                        not null,
   TENCHUDE             varchar(50)                    null,
   constraint PK_CHU__E primary key (MACHUDE)
);

/*==============================================================*/
/* Index: CHU__E_PK                                             */
/*==============================================================*/
create unique index CHU__E_PK on CHU__E (
MACHUDE ASC
);

/*==============================================================*/
/* Table: HOSO_NHATUYENDUNG                                     */
/*==============================================================*/
create table HOSO_NHATUYENDUNG 
(
   MANHATUYENDUNG       char(6)                        not null,
   MAHOSO               char(6)                        not null,
   constraint PK_HOSO_NHATUYENDUNG primary key clustered (MANHATUYENDUNG, MAHOSO)
);

/*==============================================================*/
/* Index: HOSO_NHATUYENDUNG_PK                                  */
/*==============================================================*/
create unique clustered index HOSO_NHATUYENDUNG_PK on HOSO_NHATUYENDUNG (
MANHATUYENDUNG ASC,
MAHOSO ASC
);

/*==============================================================*/
/* Index: HOSO_NHATUYENDUNG_FK                                  */
/*==============================================================*/
create index HOSO_NHATUYENDUNG_FK on HOSO_NHATUYENDUNG (
MANHATUYENDUNG ASC
);

/*==============================================================*/
/* Index: HOSO_NHATUYENDUNG2_FK                                 */
/*==============================================================*/
create index HOSO_NHATUYENDUNG2_FK on HOSO_NHATUYENDUNG (
MAHOSO ASC
);

/*==============================================================*/
/* Table: HO_SO                                                 */
/*==============================================================*/
create table HO_SO 
(
   MAHOSO               char(6)                        not null,
   MANGUOITIMVIEC       char(6)                        null,
   THONGTINHOSO         long varchar                   null,
   constraint PK_HO_SO primary key (MAHOSO)
);

/*==============================================================*/
/* Index: HO_SO_PK                                              */
/*==============================================================*/
create unique index HO_SO_PK on HO_SO (
MAHOSO ASC
);

/*==============================================================*/
/* Index: _CO_FK                                                */
/*==============================================================*/
create index _CO_FK on HO_SO (
MANGUOITIMVIEC ASC
);

/*==============================================================*/
/* Table: MAN_HINH                                              */
/*==============================================================*/
create table MAN_HINH 
(
   MAMANHINH            char(5)                        not null,
   TENMANHINH           varchar(50)                    null,
   constraint PK_MAN_HINH primary key (MAMANHINH)
);

/*==============================================================*/
/* Index: MAN_HINH_PK                                           */
/*==============================================================*/
create unique index MAN_HINH_PK on MAN_HINH (
MAMANHINH ASC
);

/*==============================================================*/
/* Table: NGUOI_DUNG                                            */
/*==============================================================*/
create table NGUOI_DUNG 
(
   TAIKHOAN             varchar(20)                    not null,
   MATKHAU              varchar(20)                    null,
   HOATDONG             smallint                       null,
   constraint PK_NGUOI_DUNG primary key (TAIKHOAN)
);

/*==============================================================*/
/* Index: NGUOI_DUNG_PK                                         */
/*==============================================================*/
create unique index NGUOI_DUNG_PK on NGUOI_DUNG (
TAIKHOAN ASC
);

/*==============================================================*/
/* Table: NGUOI_DUNG__NHOM_NGUOI_DUNG                           */
/*==============================================================*/
create table NGUOI_DUNG__NHOM_NGUOI_DUNG 
(
   TAIKHOAN             varchar(20)                    not null,
   MANHOMNGUOIDUNG      char(5)                        not null,
   constraint PK_NGUOI_DUNG__NHOM_NGUOI_DUNG primary key clustered (TAIKHOAN, MANHOMNGUOIDUNG)
);

/*==============================================================*/
/* Index: NGUOI_DUNG__NHOM_NGUOI_DUNG_PK                        */
/*==============================================================*/
create unique clustered index NGUOI_DUNG__NHOM_NGUOI_DUNG_PK on NGUOI_DUNG__NHOM_NGUOI_DUNG (
TAIKHOAN ASC,
MANHOMNGUOIDUNG ASC
);

/*==============================================================*/
/* Index: NGUOI_DUNG__NHOM_NGUOI_DUNG_FK                        */
/*==============================================================*/
create index NGUOI_DUNG__NHOM_NGUOI_DUNG_FK on NGUOI_DUNG__NHOM_NGUOI_DUNG (
TAIKHOAN ASC
);

/*==============================================================*/
/* Index: NGUOI_DUNG__NHOM_NGUOI_DUNG2_FK                       */
/*==============================================================*/
create index NGUOI_DUNG__NHOM_NGUOI_DUNG2_FK on NGUOI_DUNG__NHOM_NGUOI_DUNG (
MANHOMNGUOIDUNG ASC
);

/*==============================================================*/
/* Table: NGUOI_TIM_VIEC                                        */
/*==============================================================*/
create table NGUOI_TIM_VIEC 
(
   MANGUOITIMVIEC       char(6)                        not null,
   TAIKHOAN             varchar(20)                    not null,
   TENNGUOITIMVIEC      varchar(50)                    null,
   GIOITINH             varchar(5)                     null,
   NGAYSINH             date                           null,
   DIACHI               varchar(50)                    null,
   SDT                  varchar(11)                    null,
   EMAIL                varchar(50)                    null,
   constraint PK_NGUOI_TIM_VIEC primary key (MANGUOITIMVIEC)
);

/*==============================================================*/
/* Index: NGUOI_TIM_VIEC_PK                                     */
/*==============================================================*/
create unique index NGUOI_TIM_VIEC_PK on NGUOI_TIM_VIEC (
MANGUOITIMVIEC ASC
);

/*==============================================================*/
/* Index: __CO_FK                                               */
/*==============================================================*/
create index __CO_FK on NGUOI_TIM_VIEC (
TAIKHOAN ASC
);

/*==============================================================*/
/* Table: NHA_TUYEN_DUNG                                        */
/*==============================================================*/
create table NHA_TUYEN_DUNG 
(
   MANHATUYENDUNG       char(6)                        not null,
   TAIKHOAN             varchar(20)                    not null,
   TENNHATUYENDUNG      varchar(100)                   null,
   DIACHI               varchar(50)                    null,
   constraint PK_NHA_TUYEN_DUNG primary key (MANHATUYENDUNG)
);

/*==============================================================*/
/* Index: NHA_TUYEN_DUNG_PK                                     */
/*==============================================================*/
create unique index NHA_TUYEN_DUNG_PK on NHA_TUYEN_DUNG (
MANHATUYENDUNG ASC
);

/*==============================================================*/
/* Index: CO___FK                                               */
/*==============================================================*/
create index CO___FK on NHA_TUYEN_DUNG (
TAIKHOAN ASC
);

/*==============================================================*/
/* Table: NHOMNGUOIDUNG_THAOTAC                                 */
/*==============================================================*/
create table NHOMNGUOIDUNG_THAOTAC 
(
   MATHAOTAC            char(6)                        not null,
   MANHOMNGUOIDUNG      char(5)                        not null,
   constraint PK_NHOMNGUOIDUNG_THAOTAC primary key clustered (MATHAOTAC, MANHOMNGUOIDUNG)
);

/*==============================================================*/
/* Index: NHOMNGUOIDUNG_THAOTAC_PK                              */
/*==============================================================*/
create unique clustered index NHOMNGUOIDUNG_THAOTAC_PK on NHOMNGUOIDUNG_THAOTAC (
MATHAOTAC ASC,
MANHOMNGUOIDUNG ASC
);

/*==============================================================*/
/* Index: NHOMNGUOIDUNG_THAOTAC_FK                              */
/*==============================================================*/
create index NHOMNGUOIDUNG_THAOTAC_FK on NHOMNGUOIDUNG_THAOTAC (
MATHAOTAC ASC
);

/*==============================================================*/
/* Index: NHOMNGUOIDUNG_THAOTAC2_FK                             */
/*==============================================================*/
create index NHOMNGUOIDUNG_THAOTAC2_FK on NHOMNGUOIDUNG_THAOTAC (
MANHOMNGUOIDUNG ASC
);

/*==============================================================*/
/* Table: NHOM_NGUOI_DUNG                                       */
/*==============================================================*/
create table NHOM_NGUOI_DUNG 
(
   MANHOMNGUOIDUNG      char(5)                        not null,
   TENNHOMNGUOIDUNG     varchar(50)                    null,
   GHICHU               varchar(50)                    null,
   constraint PK_NHOM_NGUOI_DUNG primary key (MANHOMNGUOIDUNG)
);

/*==============================================================*/
/* Index: NHOM_NGUOI_DUNG_PK                                    */
/*==============================================================*/
create unique index NHOM_NGUOI_DUNG_PK on NHOM_NGUOI_DUNG (
MANHOMNGUOIDUNG ASC
);

/*==============================================================*/
/* Table: NHOM_NGUOI_DUNG___MAN_HINH                            */
/*==============================================================*/
create table NHOM_NGUOI_DUNG___MAN_HINH 
(
   MAMANHINH            char(5)                        not null,
   MANHOMNGUOIDUNG      char(5)                        not null,
   COQ__                smallint                       null,
   constraint PK_NHOM_NGUOI_DUNG___MAN_HINH primary key clustered (MAMANHINH, MANHOMNGUOIDUNG)
);

/*==============================================================*/
/* Index: NHOM_NGUOI_DUNG___MAN_HINH_PK                         */
/*==============================================================*/
create unique clustered index NHOM_NGUOI_DUNG___MAN_HINH_PK on NHOM_NGUOI_DUNG___MAN_HINH (
MAMANHINH ASC,
MANHOMNGUOIDUNG ASC
);

/*==============================================================*/
/* Index: NHOM_NGUOI_DUNG___MAN_HINH_FK                         */
/*==============================================================*/
create index NHOM_NGUOI_DUNG___MAN_HINH_FK on NHOM_NGUOI_DUNG___MAN_HINH (
MAMANHINH ASC
);

/*==============================================================*/
/* Index: NHOM_NGUOI_DUNG___MAN_HINH2_FK                        */
/*==============================================================*/
create index NHOM_NGUOI_DUNG___MAN_HINH2_FK on NHOM_NGUOI_DUNG___MAN_HINH (
MANHOMNGUOIDUNG ASC
);

/*==============================================================*/
/* Table: THAO_TAC                                              */
/*==============================================================*/
create table THAO_TAC 
(
   MATHAOTAC            char(6)                        not null,
   TENTHAOTAC           varchar(30)                    null,
   constraint PK_THAO_TAC primary key (MATHAOTAC)
);

/*==============================================================*/
/* Index: THAO_TAC_PK                                           */
/*==============================================================*/
create unique index THAO_TAC_PK on THAO_TAC (
MATHAOTAC ASC
);

/*==============================================================*/
/* Table: TINH_THANH                                            */
/*==============================================================*/
create table TINH_THANH 
(
   MATINH               char(6)                        not null,
   TENTINH              varchar(50)                    null,
   constraint PK_TINH_THANH primary key (MATINH)
);

/*==============================================================*/
/* Index: TINH_THANH_PK                                         */
/*==============================================================*/
create unique index TINH_THANH_PK on TINH_THANH (
MATINH ASC
);

/*==============================================================*/
/* Table: TIN_TUYEN_DUNG                                        */
/*==============================================================*/
create table TIN_TUYEN_DUNG 
(
   MATINTUYENDUNG       char(6)                        not null,
   MATINH               char(6)                        null,
   MANHATUYENDUNG       char(6)                        null,
   NOIDUNG              long varchar                   null,
   NGAYDANG             timestamp                      null,
   NGAYBATDAU           date                           null,
   NGAYHETHAN           date                           null,
   TINHTRANG            integer                        null,
   constraint PK_TIN_TUYEN_DUNG primary key (MATINTUYENDUNG)
);

/*==============================================================*/
/* Index: TIN_TUYEN_DUNG_PK                                     */
/*==============================================================*/
create unique index TIN_TUYEN_DUNG_PK on TIN_TUYEN_DUNG (
MATINTUYENDUNG ASC
);

/*==============================================================*/
/* Index: CO_FK                                                 */
/*==============================================================*/
create index CO_FK on TIN_TUYEN_DUNG (
MANHATUYENDUNG ASC
);

/*==============================================================*/
/* Index: THUOC_FK                                              */
/*==============================================================*/
create index THUOC_FK on TIN_TUYEN_DUNG (
MATINH ASC
);

/*==============================================================*/
/* Table: TIN_TUYEN_DUNG__CHU_DE                                */
/*==============================================================*/
create table TIN_TUYEN_DUNG__CHU_DE 
(
   MACHUDE              char(6)                        not null,
   MATINTUYENDUNG       char(6)                        not null,
   constraint PK_TIN_TUYEN_DUNG__CHU_DE primary key clustered (MACHUDE, MATINTUYENDUNG)
);

/*==============================================================*/
/* Index: TIN_TUYEN_DUNG__CHU_DE_PK                             */
/*==============================================================*/
create unique clustered index TIN_TUYEN_DUNG__CHU_DE_PK on TIN_TUYEN_DUNG__CHU_DE (
MACHUDE ASC,
MATINTUYENDUNG ASC
);

/*==============================================================*/
/* Index: TIN_TUYEN_DUNG__CHU_DE_FK                             */
/*==============================================================*/
create index TIN_TUYEN_DUNG__CHU_DE_FK on TIN_TUYEN_DUNG__CHU_DE (
MACHUDE ASC
);

/*==============================================================*/
/* Index: TIN_TUYEN_DUNG__CHU_DE2_FK                            */
/*==============================================================*/
create index TIN_TUYEN_DUNG__CHU_DE2_FK on TIN_TUYEN_DUNG__CHU_DE (
MATINTUYENDUNG ASC
);

alter table HOSO_NHATUYENDUNG
   add constraint FK_HOSO_NHA_HOSO_NHAT_NHA_TUYE foreign key (MANHATUYENDUNG)
      references NHA_TUYEN_DUNG (MANHATUYENDUNG)
      on update restrict
      on delete restrict;

alter table HOSO_NHATUYENDUNG
   add constraint FK_HOSO_NHA_HOSO_NHAT_HO_SO foreign key (MAHOSO)
      references HO_SO (MAHOSO)
      on update restrict
      on delete restrict;

alter table HO_SO
   add constraint FK_HO_SO__CO_NGUOI_TI foreign key (MANGUOITIMVIEC)
      references NGUOI_TIM_VIEC (MANGUOITIMVIEC)
      on update restrict
      on delete restrict;

alter table NGUOI_DUNG__NHOM_NGUOI_DUNG
   add constraint FK_NGUOI_DU_NGUOI_DUN_NGUOI_DU foreign key (TAIKHOAN)
      references NGUOI_DUNG (TAIKHOAN)
      on update restrict
      on delete restrict;

alter table NGUOI_DUNG__NHOM_NGUOI_DUNG
   add constraint FK_NGUOI_DU_NGUOI_DUN_NHOM_NGU foreign key (MANHOMNGUOIDUNG)
      references NHOM_NGUOI_DUNG (MANHOMNGUOIDUNG)
      on update restrict
      on delete restrict;

alter table NGUOI_TIM_VIEC
   add constraint FK_NGUOI_TI___CO_NGUOI_DU foreign key (TAIKHOAN)
      references NGUOI_DUNG (TAIKHOAN)
      on update restrict
      on delete restrict;

alter table NHA_TUYEN_DUNG
   add constraint FK_NHA_TUYE_CO___NGUOI_DU foreign key (TAIKHOAN)
      references NGUOI_DUNG (TAIKHOAN)
      on update restrict
      on delete restrict;

alter table NHOMNGUOIDUNG_THAOTAC
   add constraint FK_NHOMNGUO_NHOMNGUOI_THAO_TAC foreign key (MATHAOTAC)
      references THAO_TAC (MATHAOTAC)
      on update restrict
      on delete restrict;

alter table NHOMNGUOIDUNG_THAOTAC
   add constraint FK_NHOMNGUO_NHOMNGUOI_NHOM_NGU foreign key (MANHOMNGUOIDUNG)
      references NHOM_NGUOI_DUNG (MANHOMNGUOIDUNG)
      on update restrict
      on delete restrict;

alter table NHOM_NGUOI_DUNG___MAN_HINH
   add constraint FK_NHOM_NGU_NHOM_NGUO_MAN_HINH foreign key (MAMANHINH)
      references MAN_HINH (MAMANHINH)
      on update restrict
      on delete restrict;

alter table NHOM_NGUOI_DUNG___MAN_HINH
   add constraint FK_NHOM_NGU_NHOM_NGUO_NHOM_NGU foreign key (MANHOMNGUOIDUNG)
      references NHOM_NGUOI_DUNG (MANHOMNGUOIDUNG)
      on update restrict
      on delete restrict;

alter table TIN_TUYEN_DUNG
   add constraint FK_TIN_TUYE_CO_NHA_TUYE foreign key (MANHATUYENDUNG)
      references NHA_TUYEN_DUNG (MANHATUYENDUNG)
      on update restrict
      on delete restrict;

alter table TIN_TUYEN_DUNG
   add constraint FK_TIN_TUYE_THUOC_TINH_THA foreign key (MATINH)
      references TINH_THANH (MATINH)
      on update restrict
      on delete restrict;

alter table TIN_TUYEN_DUNG__CHU_DE
   add constraint FK_TIN_TUYE_TIN_TUYEN_CHU__E foreign key (MACHUDE)
      references CHU__E (MACHUDE)
      on update restrict
      on delete restrict;

alter table TIN_TUYEN_DUNG__CHU_DE
   add constraint FK_TIN_TUYE_TIN_TUYEN_TIN_TUYE foreign key (MATINTUYENDUNG)
      references TIN_TUYEN_DUNG (MATINTUYENDUNG)
      on update restrict
      on delete restrict;

