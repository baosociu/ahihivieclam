package hufi.gjobs.Views.Report;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hufi.gjobs.Master.GJobsFragment;
import hufi.gjobs.Model.Post;
import hufi.gjobs.Model.Report;
import hufi.gjobs.R;
import hufi.gjobs.Service.FirebasePost;
import hufi.gjobs.Views.RecyclerView.RecyclerViewPostAdapter;

import static hufi.gjobs.Master.GlobalConstant.COUNT_PAGE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReportFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReportFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReportFragment extends GJobsFragment implements FloatingSearchView.OnQueryChangeListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public ReportFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReportFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReportFragment newInstance(String param1, String param2) {
        ReportFragment fragment = new ReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @BindView(R.id.floating_search_view_report)
    public FloatingSearchView mSearchView;
    @BindView(R.id.recyclerViewReport)
    public RecyclerView recyclerView;
    private RecyclerViewPostAdapter adapter;
    List<Post> data;
    List<Report> dataReport;
    int page = 1;
    private ReportFragment _fragment;
    private boolean isFirstTime;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_report, container, false);
        ButterKnife.bind(this, v);
        setHasOptionsMenu(true);
        _fragment = this;
        isFirstTime = true;

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));
        recyclerView.setHasFixedSize(true);

        LoadingData("", page);


        mSearchView.setOnQueryChangeListener(this);
        return v;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @OnClick(R.id.btnLoadMoreReport)
    public void LoadMore() {
        if (page * COUNT_PAGE > data.size() || page * COUNT_PAGE > dataReport.size())
            Toast.makeText(getContext(), getResources().getString(R.string.ComingSoonDisplay), Toast.LENGTH_SHORT).show();
        else {
            page += 1;
            myActivity.showLoading();
            LoadingData(mSearchView.getQuery(), page);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onSearchTextChanged(String oldQuery, String newQuery) {
        page = 1;
        LoadingData(newQuery, page);
    }

    void LoadingData(String searchKey, int Page) {
        final FirebasePost firebasePost = new FirebasePost(getContext());
        firebasePost.getReportPost(searchKey);
        final Handler handler = new Handler();
        if(isFirstTime)
        {
            myActivity.showLoading();
            isFirstTime = false;
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (firebasePost.isComplete()) {
                    if (firebasePost.isSuccessfully()) {
                        List<Post> output = (ArrayList<Post>)((Object[]) firebasePost.getData())[0];
                        List<Report> outputReport = (ArrayList<Report>) ((Object[]) firebasePost.getData())[1];
                        //phân trang
                        data = new ArrayList<Post>();
                        dataReport = new ArrayList<Report>();

                        for (int i = 0; i < output.size()  && i < outputReport.size()
                                && data.size() <= page * COUNT_PAGE && dataReport.size() <= page * COUNT_PAGE; i++)
                        {
                            data.add(output.get(i));
                            dataReport.add(outputReport.get(i));
                        }
                        adapter = new RecyclerViewPostAdapter(_fragment, new Object[]{data,dataReport});
                        recyclerView.setAdapter(adapter);

                        Handler handlerAdapter = new Handler();
                        handlerAdapter.post(new Runnable() {
                            @Override
                            public void run() {
                                if(adapter.isCompleted())
                                {
                                    myActivity.hideLoading();
                                    handler.removeCallbacks(this);
                                }
                                else
                                    handler.post(this);
                            }
                        });
                    }
                    else
                        myActivity.hideLoading();
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        });
    }
}
