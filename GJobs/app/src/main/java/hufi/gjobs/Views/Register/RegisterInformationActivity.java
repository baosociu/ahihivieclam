package hufi.gjobs.Views.Register;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.philliphsu.bottomsheetpickers.date.DatePickerDialog;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hufi.gjobs.Master.GJobsActivity;
import hufi.gjobs.Master.GJobsFunction;
import hufi.gjobs.Model.Account;
import hufi.gjobs.Model.Man;
import hufi.gjobs.R;
import hufi.gjobs.Service.FirebaseAccount;
import hufi.gjobs.Service.FirebaseMan;
import hufi.gjobs.Utils.DrawableUtils;
import hufi.gjobs.Utils.Utils;
import hufi.gjobs.Views.Login.LoginActivity;

public class RegisterInformationActivity extends GJobsActivity implements DatePickerDialog.OnDateSetListener {
    @BindView(R.id.relativeLayoutRegisterInformation)
    public RelativeLayout relativeLayout;
    @BindView(R.id.edtAddressRegInfo)
    public EditText address;
    @BindView(R.id.edtNameRegInfo)
    public EditText name;
    @BindView(R.id.edtNumberphoneRegInfo)
    public EditText numberphone;
    @BindView(R.id.txtDateofBirthRegInfo)
    public TextView dob;
    @BindView(R.id.txtProvinceRegInfo)
    public TextView province;
    @BindView(R.id.txtTypeAccountRegInfo)
    public TextView typeaccount;
    @BindView(R.id.radioMaleRegInfo)
    public RadioButton raMale;
    @BindView(R.id.icDateOfBirthRegInfo)
    public ImageView icBirth;
    private String username;
    private String email;
    private String pass;
    MaterialDialog provinceDialog, typeAccountDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_information);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getBundleExtra("mBundle");
        username = bundle.getString("mUsername", "");
        pass = bundle.getString("mPassword", "");
        email = bundle.getString("mEmail", "");
        settingLayout();
        setLoading(relativeLayout);
    }

    private void settingLayout() {
        icBirth.setImageDrawable(DrawableUtils.getIconAwesome(this, FontAwesome.Icon.faw_calendar, getResources().getColor(R.color.primary_color)));
        raMale.setChecked(true);
        address.setText("");
        numberphone.setText("");
        name.setText("");
        dob.setText(Utils.dateToString(Calendar.getInstance().getTime(), "dd/MM/yyyy"));

        province.setTag(0);
        settingProvincePicker();
        province.setText(provinceDialog.getItems().get(0).toString());
        typeaccount.setTag(0);
        settingTypeAccountPicker();
        typeaccount.setText(typeAccountDialog.getItems().get(0).toString());

        settingDatePicker();
    }

    @OnClick(R.id.txtProvinceRegInfo)
    public void showProvince() {
        //LẤY tag để thay thế getSelectedIndex
        int index = (int)province.getTag();
        provinceDialog.setSelectedIndex(index);
        provinceDialog.show();
    }

    @OnClick(R.id.txtTypeAccountRegInfo)
    public void showTypeAccount() {
        //LẤY tag để thay thế getSelectedIndex
        int index = (int)typeaccount.getTag();
        typeAccountDialog.setSelectedIndex(index);
        typeAccountDialog.show();
    }

    private void settingProvincePicker(){
        List<String> provinces = Arrays.asList(getResources().getStringArray(R.array.arrProvince));
        provinceDialog = new MaterialDialog.Builder(this)
                .title(R.string.province)
                .titleColorRes(R.color.primary_color)
                .items(provinces)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        return true;
                    }
                })
                .titleColorRes(R.color.primary_color)
                .positiveText(R.string.choose)
                .positiveColorRes(R.color.primary_color)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        String str = dialog.getItems().get(dialog.getSelectedIndex()).toString();
                        province.setText(str);
                        province.setTag(dialog.getSelectedIndex());
                    }
                })
                .build();
        provinceDialog.setSelectedIndex(Integer.parseInt(province.getTag().toString()));
    }

    private void settingTypeAccountPicker(){
        List<String> provinces = Arrays.asList(getResources().getStringArray(R.array.arrTypeAccount));
        typeAccountDialog = new MaterialDialog.Builder(this)
                .title(R.string.type_acount)
                .titleColorRes(R.color.primary_color)
                .items(provinces)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        return true;
                    }
                })
                .titleColorRes(R.color.primary_color)
                .positiveText(R.string.choose)
                .positiveColorRes(R.color.primary_color)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        String str = dialog.getItems().get(dialog.getSelectedIndex()).toString();
                        typeaccount.setText(str);
                        typeaccount.setTag(dialog.getSelectedIndex());
                    }
                })
                .build();
        typeAccountDialog.setSelectedIndex(Integer.parseInt(typeaccount.getTag().toString()));
    }

    private void settingDatePicker() {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int day = now.get(Calendar.DAY_OF_MONTH);

        final DatePickerDialog date = new DatePickerDialog.Builder(this, year, month, day).build();
        date.setAccentColor(getResources().getColor(R.color.primary_color));
        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date.show(getSupportFragmentManager(), getResources().getString(R.string.item_register_complete));
            }
        });
    }

    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        Calendar cal = new java.util.GregorianCalendar();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        dob.setText(Utils.dateToString(cal.getTime(), "dd/MM/yyyy"));
    }

    public void RegisterAccountAndInfor() {
        int indexTypeAccount = (int)typeaccount.getTag();
        Account a = new Account(username, pass, indexTypeAccount, email);

        //tạo mã kích hoạt
        final String activedCode = GJobsFunction.getActiveCode();
        a.setActiveCode(activedCode);

        //todo thêm tài khoản
        final FirebaseAccount fireaccount = new FirebaseAccount(this);
        fireaccount.addAccount(a);
        final Handler hadler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (fireaccount.isComplete()) {
                    Toast.makeText(getBaseContext(), fireaccount.getMessageResult(), Toast.LENGTH_SHORT).show();
                    if (fireaccount.isSuccessfully()) {
                        Account acc = (Account) fireaccount.getData();
                        RegisterMan(acc);
                    } else
                        hideLoading();
                    hadler.removeCallbacks(this);
                } else
                    hadler.post(this);
            }
        };
        hadler.post(runnable);
    }


    private void RegisterMan(final Account acc) {
        int type = (int)typeaccount.getTag();
        String strAddress = address.getText().toString();
        String strNumberphone = numberphone.getText().toString();
        String strName = name.getText().toString();
        int idProvince = (int)province.getTag();
        Date d = Utils.stringToDate(dob.getText().toString(), "dd/MM/yyyy");
        final FirebaseMan firebaseMan = new FirebaseMan(this, type);

        Man man = new Man(acc.getIdAccount(), strName, strAddress, idProvince, strNumberphone, email, "", d, raMale.isChecked());
        firebaseMan.addMan(man);

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (firebaseMan.isComplete()) {
                    //Toast.makeText(getBaseContext(), firebaseMan.getMessageResult(), Toast.LENGTH_SHORT).show();
                    if (firebaseMan.isSuccessfully())
                    {
                        GJobsFunction.sendMailVerification(RegisterInformationActivity.this,
                                email, acc.getActiveCode(),username);
                        //todo: quay về đăng nhập
                        Intent i = new Intent(RegisterInformationActivity.this, LoginActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("myUsername",username);
                        bundle.putString("myPassword",pass);
                        i.putExtra("myBundle",bundle);
                        startActivity(i);
                    }
                    hideLoading();
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        handler.post(runnable);
    }


    @OnClick(R.id.btnCompleteRegister)
    public void onClickCompleteRegister() {
        String strAddress = address.getText().toString();
        String strNumberphone = numberphone.getText().toString();
        String strName = name.getText().toString();

        if (strAddress.isEmpty() || strNumberphone.isEmpty() || strName.isEmpty()) {
            if (strAddress.isEmpty())
                address.setError(getResources().getString(R.string.Information_Require));
            if (strNumberphone.isEmpty())
                numberphone.setError(getResources().getString(R.string.Information_Require));
            if (strName.isEmpty())
                name.setError(getResources().getString(R.string.Information_Require));
            return;
        }

        //kiểm tra lại email và username có người đăng ksy chưa ?
        final FirebaseAccount firebaseAccount = new FirebaseAccount(this);
        firebaseAccount.findAccountByUsernameAndEmail(username, email);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (firebaseAccount.isComplete()) {
                    if (firebaseAccount.isSuccessfully()){
                        Toast.makeText(getBaseContext(), getResources().getString(R.string.Register_Duplication), Toast.LENGTH_SHORT).show();
                        hideLoading();
                        onBackPressed();
                    }
                    else
                        RegisterAccountAndInfor();
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        showLoading();
        handler.post(runnable);
    }

}