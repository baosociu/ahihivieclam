package hufi.gjobs.Views.Candidate;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hufi.gjobs.Master.GJobsFragment;
import hufi.gjobs.Model.Account;
import hufi.gjobs.Model.Apply;
import hufi.gjobs.R;
import hufi.gjobs.Service.FirebaseAccount;
import hufi.gjobs.Views.RecyclerView.RecyclerViewUserAdapter;

import static hufi.gjobs.Master.GlobalConstant.COUNT_PAGE;
import static hufi.gjobs.Master.GlobalConstant.account;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CandidateFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CandidateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CandidateFragment extends GJobsFragment implements FloatingSearchView.OnQueryChangeListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public CandidateFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CandidateFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CandidateFragment newInstance(String param1, String param2) {
        CandidateFragment fragment = new CandidateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @BindView(R.id.floating_search_view_candidate)
    public FloatingSearchView mSearchView;
    @BindView(R.id.recyclerViewCandidate)
    public RecyclerView recyclerView;

    private RecyclerViewUserAdapter adapter;
    List<Account> data;
    List<Apply> dataApply;
    int page = 1;
    private CandidateFragment _fragment;
    private boolean isFirstTime;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_candidate, container, false);
        ButterKnife.bind(this,v);
        setHasOptionsMenu(true);
        _fragment = this;
        isFirstTime = true;

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));
        recyclerView.setHasFixedSize(true);

        LoadingData("",page);


        mSearchView.setOnQueryChangeListener(this);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @OnClick(R.id.btnLoadMoreCandidate)
    public void LoadMore() {
        if (page * COUNT_PAGE > data.size())
            Toast.makeText(getContext(), getResources().getString(R.string.ComingSoonDisplayUser), Toast.LENGTH_SHORT).show();
        else {
            page += 1;
            myActivity.showLoading();
            LoadingData(mSearchView.getQuery(),page);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onSearchTextChanged(String oldQuery, String newQuery) {
        page = 1;
        LoadingData(newQuery, page);
    }

    void LoadingData(String searchKey, int Page) {
        final FirebaseAccount firebaseAccount = new FirebaseAccount(getContext());
        firebaseAccount.getAppliedAccount(account.getIdAccount(), searchKey);
        final Handler handler = new Handler();
        if(isFirstTime)
        {
            myActivity.showLoading();
            isFirstTime = false;
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (firebaseAccount.isComplete()) {
                    if (firebaseAccount.isSuccessfully()) {
                        List<Account> output = (ArrayList<Account>) ((Object[])firebaseAccount.getData())[0];
                        List<Apply> outputApply = (ArrayList<Apply>) ((Object[])firebaseAccount.getData())[1];
                        //phân trang
                        data = new ArrayList<Account>();
                        dataApply = new ArrayList<Apply>();

                        for (int i = 0; i < output.size() && data.size() <= page*COUNT_PAGE; i++){
                            data.add(output.get(i));
                            dataApply.add(outputApply.get(i));
                        }
                        adapter = new RecyclerViewUserAdapter(_fragment, data,dataApply);
                        recyclerView.setAdapter(adapter);

                        Handler handlerAdapter = new Handler();
                        handlerAdapter.post(new Runnable() {
                            @Override
                            public void run() {
                                if(adapter.isCompleted())
                                {
                                    myActivity.hideLoading();
                                    handler.removeCallbacks(this);
                                }
                                else
                                    handler.post(this);
                            }
                        });
                    }
                    else
                        myActivity.hideLoading();
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        });
    }
}
