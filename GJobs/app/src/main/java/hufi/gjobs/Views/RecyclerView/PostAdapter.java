package hufi.gjobs.Views.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hufi.gjobs.Master.GJobsFragment;
import hufi.gjobs.Master.GlobalConstant;
import hufi.gjobs.Model.Account;
import hufi.gjobs.Model.Man;
import hufi.gjobs.Model.Post;
import hufi.gjobs.R;
import hufi.gjobs.Service.FirebaseMan;
import hufi.gjobs.Utils.BitmapUtils;
import hufi.gjobs.Utils.Go;
import hufi.gjobs.Utils.Utils;
import hufi.gjobs.Views.Detail.DetailPostFragment;
import hufi.gjobs.Views.Post.PostFragment;

/**
 * Created by baobao1996mn on 17/10/2017.
 */

public class PostAdapter extends FirebaseRecyclerAdapter<Post, PostAdapter.PostViewHolder> {
    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    List<String> provinces;
    Context context;
    Fragment fragment;

    public PostAdapter(Fragment fragment, FirebaseRecyclerOptions<Post> options) {
        super(options);
        this.context = fragment.getContext();
        this.fragment = fragment;
        provinces = Arrays.asList(context.getResources().getStringArray(R.array.arrProvince));
    }


    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 0)
            ((GJobsFragment)fragment).myActivity.hideLoading();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post_layout, null, false);
        PostViewHolder viewHolder = new PostViewHolder(v);
        return viewHolder;
    }

    @Override
    protected void onBindViewHolder(final PostViewHolder holder, int position, final Post post) {

        holder.txtTitle.setText(post.getIdPost());
        holder.txtAddress.setText(provinces.get(post.getIdProvince()));
        holder.txtRecuit.setText(post.getIdReciut());
        holder.txtDate.setText(Utils.dateToString(post.getDatePost(), "dd/MM/yyyy"));
        //todo lấy thông tin của nhà tuyển dụng
        final FirebaseMan firebaseMan = new FirebaseMan(context, Account.TYPE_BOSS);
        firebaseMan.findManByKey(post.getIdReciut());

        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (firebaseMan.isComplete()) {
                    if (firebaseMan.isSuccessfully()) {
                        Man boss = (Man) firebaseMan.getData();
                        if (boss.getImage() != null && !boss.getImage().isEmpty())
                            holder.imagePost.setImageBitmap(BitmapUtils.decodeBase64(boss.getImage()));
                        else
                            holder.imagePost.setImageDrawable(context.getResources().getDrawable(R.drawable.gjobsjpg));
                    }
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo: Type_post: nhà tuyển dụng thêm bài đăng mới
                DetailPostFragment detailPostFragment = new DetailPostFragment();
                Bundle bundle = new Bundle();

                int idType = -1;

                if(GlobalConstant.account.getTypeAccount() == Account.TYPE_ADMIN)
                    idType = R.string.TYPE_POST_ADMIN_DETAIL;
                else if(GlobalConstant.account.getTypeAccount() == Account.TYPE_BOSS)
                {
                    if(fragment instanceof PostFragment)
                        idType = R.string.TYPE_POST_RECUIT_EDIT; //bài của nahf tuyển dụng
                    else
                        idType = R.string.TYPE_POST_RECUIT_DETAIL;
                }
                else if(GlobalConstant.account.getTypeAccount() == Account.TYPE_CANDIDATE)
                    idType = R.string.TYPE_POST_USER;

                if(idType !=-1)
                    bundle.putString("TYPE_POST",context.getResources().getString(idType));

                bundle.putString("ID_POST",post.getIdPost());
                bundle.putString("mTitle",context.getResources().getString(R.string.item_detail_post));
                bundle.putBoolean("mArrow",true);
                detailPostFragment.setArguments(bundle);
                Go.Add((AppCompatActivity)context,detailPostFragment,R.string.item_detail_post);
            }
        });
    }


    protected class PostViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_post_address)
        TextView txtAddress;
        @BindView(R.id.item_post_date)
        TextView txtDate;
        @BindView(R.id.item_post_recruit)
        TextView txtRecuit;
        @BindView(R.id.item_post_title)
        TextView txtTitle;
        @BindView(R.id.item_post_image)
        ImageView imagePost;

        public PostViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
