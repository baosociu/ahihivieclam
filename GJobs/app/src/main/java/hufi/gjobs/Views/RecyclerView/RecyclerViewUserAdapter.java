package hufi.gjobs.Views.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hufi.gjobs.Master.GlobalConstant;
import hufi.gjobs.Model.Account;
import hufi.gjobs.Model.Apply;
import hufi.gjobs.Model.Man;
import hufi.gjobs.Model.Post;
import hufi.gjobs.R;
import hufi.gjobs.Service.FirebaseMan;
import hufi.gjobs.Service.FirebasePost;
import hufi.gjobs.Utils.BitmapUtils;
import hufi.gjobs.Utils.DrawableUtils;
import hufi.gjobs.Utils.Go;
import hufi.gjobs.Views.Detail.DetailUserFragment;

import static hufi.gjobs.Master.GlobalConstant.COUNT_MIN;

/**
 * Created by baobao1996mn on 12/11/2017.
 */

public class RecyclerViewUserAdapter extends RecyclerView.Adapter<RecyclerViewUserAdapter.UserViewHolder> {
    List<UserViewHolder> listViewHolder;
    List<String> provinces;
    Context context;
    Fragment fragment;
    List<Account> data;
    List<Apply> applies;
    int count = 0;
    boolean isCompleted;
    int idType;
    public RecyclerViewUserAdapter(Fragment fragment, List<Account> list, List<Apply> applies) {
        this.context = fragment.getContext();
        this.fragment = fragment;
        this.data = list;
        this.applies = applies;
        provinces = Arrays.asList(context.getResources().getStringArray(R.array.arrProvince));
        listViewHolder = new ArrayList<>();

        idType = -1;

        //todo người dùng hiện tại
        if(GlobalConstant.account.getTypeAccount() == Account.TYPE_ADMIN)
            idType = R.string.TYPE_DETAIL_USER_ADMIN;
        else if(GlobalConstant.account.getTypeAccount() == Account.TYPE_BOSS)
            idType = R.string.TYPE_DETAIL_USER_APPLY;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_layout, null, false);
        UserViewHolder viewHolder = new UserViewHolder(v);
        if(!isCompleted)
            v.setVisibility(View.INVISIBLE);
        listViewHolder.add(viewHolder);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final UserViewHolder holder, final int position) {
        final Account account = data.get(position);
        holder.txtUsername.setText(account.getUsername());
        settingStatusPost(holder.iconStatus,holder.txtStatus,account.getStatus());

        //todo lấy thông tin của tài khoản
        final FirebaseMan firebaseMan = new FirebaseMan(context,account.getTypeAccount());
        firebaseMan.findManByKey(account.getIdAccount());
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (firebaseMan.isComplete()) {
                    if (firebaseMan.isSuccessfully()) {
                        count++;
                        Man man = (Man) (firebaseMan.getData());
                        holder.settingLayout(account,man);
                    }
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        });

        if(applies.size() != 0) {
            Apply apply = applies.get(position);
            final FirebasePost firebasePost = new FirebasePost(context);
            firebasePost.findPostByID(apply.getIdPost());
            final Handler handler2 = new Handler();
            handler2.post(new Runnable() {
                @Override
                public void run() {
                    if(firebasePost.isComplete()){
                        if(firebasePost.isSuccessfully())
                        {
                            Post post = (Post)(firebasePost).getData();
                            holder.txtPost.setVisibility(View.VISIBLE);
                            holder.txtPost.setText(post.getTitlePost());
                            holder.lblPost.setVisibility(View.VISIBLE);
                            holder.lblPost.setText(context.getResources().getString(R.string.post)+" ");
                        }
                        handler2.removeCallbacks(this);
                    }
                    else
                        handler2.post(this);
                }
            });
        }
    }


    public boolean isCompleted(){
        isCompleted = count == data.size() || count >=COUNT_MIN;
        if(isCompleted)
            for(UserViewHolder u : listViewHolder)
                u.itemView.setVisibility(View.VISIBLE);
        return isCompleted;
    }



    private void settingStatusPost(ImageView viewStatus, TextView txtStatus, int status) {
        if (status == Account.STATUS_ACTIVED) {
            viewStatus.setImageDrawable(DrawableUtils.getIconAwesome(context, FontAwesome.Icon.faw_check,context.getResources().getColor(R.color.primary_color)));
            viewStatus.setVisibility(View.VISIBLE);
            txtStatus.setText(context.getResources().getString(R.string.StatusActivedAccount));
        } else if (status == Account.STATUS_DISACTIVED) {
            viewStatus.setImageDrawable(DrawableUtils.getIconAwesome(context, FontAwesome.Icon.faw_podcast,context.getResources().getColor(R.color.primary_color)));
            viewStatus.setVisibility(View.VISIBLE);
            txtStatus.setText(context.getResources().getString(R.string.StatusDisactivedAccount));
        } else if (status == Account.STATUS_BLOCKED) {
            viewStatus.setImageDrawable(DrawableUtils.getIconAwesome(context, FontAwesome.Icon.faw_lock,context.getResources().getColor(R.color.primary_color)));
            viewStatus.setVisibility(View.VISIBLE);
            txtStatus.setText(context.getResources().getString(R.string.StatusBlockedAccount));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    protected class UserViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_user_user)
        TextView txtUsername;
        @BindView(R.id.item_user_name)
        TextView txtName;
        @BindView(R.id.item_user_name_icon)
        ImageView iconName;
        @BindView(R.id.item_user_province)
        TextView txtProvince;
        @BindView(R.id.item_user_email)
        TextView txtEmail;
        @BindView(R.id.item_user_email_icon)
        ImageView iconEmail;
        @BindView(R.id.item_user_status)
        TextView txtStatus;
        @BindView(R.id.item_user_status_icon)
        ImageView iconStatus;
        @BindView(R.id.item_user_image)
        ImageView imageUser;
        @BindView(R.id.item_user_post)
        TextView txtPost;
        @BindView(R.id.item_user_lbl_post)
        TextView lblPost;
        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            txtPost.setVisibility(View.GONE);
            lblPost.setVisibility(View.GONE);
        }

        public void settingLayout(final Account account, final Man man){
            if (!man.getImage().isEmpty())
                imageUser.setImageBitmap(BitmapUtils.getCircleBitmap(BitmapUtils.decodeBase64(man.getImage())));
            else
                imageUser.setImageDrawable(context.getResources().getDrawable(R.drawable.gjobsjpg));

            txtName.setText(man.getNameMan());
            txtProvince.setText(provinces.get( man.getIdProvince()));
            txtEmail.setText(man.getEmail());

            iconEmail.setImageDrawable(DrawableUtils.getIconAwesome(context, FontAwesome.Icon.faw_envelope,context.getResources().getColor(R.color.primary_color)));
            iconName.setImageDrawable(DrawableUtils.getIconAwesome(context, FontAwesome.Icon.faw_user,context.getResources().getColor(R.color.primary_color)));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //todo: Type_user:
                    DetailUserFragment detailUserFragment = new DetailUserFragment();
                    Bundle bundle = new Bundle();

                    if(idType !=-1)
                        bundle.putString("TYPE_DETAIL",context.getResources().getString(idType));

                    bundle.putString("ID_MAN",man.getIdMan()); //id người dùng cần xem
                    bundle.putInt("ID_TYPE",account.getTypeAccount()); //type người dfung cần xem
                    bundle.putString("mTitle",context.getResources().getString(R.string.item_detail_user));
                    bundle.putBoolean("mArrow",true);

                    detailUserFragment.setArguments(bundle);
                    Go.Add((AppCompatActivity)context,detailUserFragment,R.string.item_detail_user);
                }
            });
        }
    }
}
