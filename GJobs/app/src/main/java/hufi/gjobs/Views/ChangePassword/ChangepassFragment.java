package hufi.gjobs.Views.ChangePassword;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import hufi.gjobs.Master.GJobsFragment;
import hufi.gjobs.Master.GlobalConstant;
import hufi.gjobs.Model.Account;
import hufi.gjobs.R;
import hufi.gjobs.Service.FirebaseAccount;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChangepassFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChangepassFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChangepassFragment extends GJobsFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ChangepassFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChangepassFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChangepassFragment newInstance(String param1, String param2) {
        ChangepassFragment fragment = new ChangepassFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.edtPasswordChangePass)
    public EditText edtPassOld;
    @BindView(R.id.edtNewPasswordChangePass)
    public EditText edtPassNew;
    @BindView(R.id.edtConfirmPasswordChangePass)
    public EditText edtPassNew2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_changepass, container, false);
        ButterKnife.bind(this, v);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_changepass, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_save_menu_changepass) {
            String passOld = edtPassOld.getText().toString();
            String passNew = edtPassNew.getText().toString();
            String passNew2 = edtPassNew2.getText().toString();
            ChangePass(passOld, passNew, passNew2);
            return true;
        } else
            return
                    super.onOptionsItemSelected(item);
    }

    private void ChangePass(String passOld, String passNew, String passNew2) {
        //todo check rỗng
        if (passNew.isEmpty() || passOld.isEmpty() || passNew2.isEmpty())
            Toast.makeText(getContext(), getResources().getString(R.string.Complete_Require), Toast.LENGTH_LONG).show();
        else {
            if (!passOld.equals(GlobalConstant.account.getPassword()))
                Toast.makeText(getContext(), getResources().getString(R.string.PassChange_Wrong_Pass), Toast.LENGTH_LONG).show();
            else {
                if (!passNew.equals(passNew2))
                    Toast.makeText(getContext(), getResources().getString(R.string.PassChange_Confirm_Wrong), Toast.LENGTH_LONG).show();
                else {
                    if (passNew.length() <= 6)
                        Toast.makeText(getContext(), getResources().getString(R.string.PassChange_Min_Length), Toast.LENGTH_LONG).show();
                    else {
                        final FirebaseAccount ac = new FirebaseAccount(getContext());
                        Account a = new Account(GlobalConstant.account);
                        a.setPassword(passNew);

                        ac.update(a,FirebaseAccount.QUERY_CHANGPASS);
                        final Handler handler = new Handler();
                        Runnable runnable = new Runnable() {
                            @Override
                            public void run() {
                                if (ac.isComplete()) {
                                    Toast.makeText(getContext(), ac.getMessageResult(), Toast.LENGTH_LONG).show();
                                    if (ac.isSuccessfully())
                                        GlobalConstant.account = (Account) ac.getData();
                                    handler.removeCallbacks(this);
                                } else
                                    handler.post(this);
                            }
                        };
                        handler.post(runnable);
                    }
                }
            }
        }
    }
}
