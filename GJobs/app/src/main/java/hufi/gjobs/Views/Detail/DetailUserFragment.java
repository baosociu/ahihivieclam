package hufi.gjobs.Views.Detail;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hufi.gjobs.Master.GJobsActivity;
import hufi.gjobs.Master.GJobsFragment;
import hufi.gjobs.Master.GlobalConstant;
import hufi.gjobs.Model.Account;
import hufi.gjobs.Model.Man;
import hufi.gjobs.R;
import hufi.gjobs.Service.FirebaseAccount;
import hufi.gjobs.Service.FirebaseMan;
import hufi.gjobs.Service.FirebaseResponse;
import hufi.gjobs.Utils.BitmapUtils;
import hufi.gjobs.Utils.DrawableUtils;
import hufi.gjobs.Utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetailUserFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetailUserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailUserFragment extends GJobsFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public DetailUserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailUserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailUserFragment newInstance(String param1, String param2) {
        DetailUserFragment fragment = new DetailUserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            ID_MAN = getArguments().getString("ID_MAN", "");
            ID_TYPE = getArguments().getInt("ID_TYPE", -1);
            TYPE_DETAIL = getArguments().getString("TYPE_DETAIL", "");
        }
    }

    @BindView(R.id.txtEmailUserInfo)
    public TextView txtEmail;
    @BindView(R.id.txtNameUserInfo)
    public TextView txtName;
    @BindView(R.id.accountNameUserInfo)
    public TextView txtAccountName;
    @BindView(R.id.txtDateofBirthUserInfo)
    public TextView txtDoB;
    @BindView(R.id.edtNumberphoneUserInfo)
    public EditText edtNumberPhone;
    @BindView(R.id.edtAddressUserInfo)
    public EditText edtAddress;
    @BindView(R.id.radioMaleUserInfo)
    public RadioButton raMale;
    @BindView(R.id.radioFemaleUserInfo)
    public RadioButton raFemale;
    @BindView(R.id.icDateOfBirthUserInfo)
    public ImageView icDate;
    @BindView(R.id.icNameUserInfo)
    public ImageView icUser;
    @BindView(R.id.icJobUserInfo)
    public ImageView icJob;
    @BindView(R.id.icEmailUserInfo)
    public ImageView icEmail;
    @BindView(R.id.imageUserInfo)
    public ImageView imageAvatar;
    @BindView(R.id.btnApply)
    public Button btnApply;
    @BindView(R.id.txtProvince)
    public TextView txtProvince;

    private String ID_MAN;
    private int ID_TYPE;
    private String TYPE_DETAIL;
    MaterialDialog dialog;
    private Man myMan;
    private Account myAccount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detail_user, container, false);
        ButterKnife.bind(this, v);
        myActivity = ((GJobsActivity) this.getActivity());
        setHasOptionsMenu(true);
        LoadInformation();

        return v;
    }

    private void LoadInformation() {
        if (!ID_MAN.isEmpty()) {
            final FirebaseMan firebaseMan = new FirebaseMan(getContext(), ID_TYPE);
            firebaseMan.findManByKey(ID_MAN);
            myActivity.showLoading();
            final Handler handler = new Handler();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (firebaseMan.isComplete()) {
                        if (firebaseMan.isSuccessfully()) {
                            myMan = (Man) firebaseMan.getData();
                            LoadInformationByMan(myMan);
                        }
                        handler.removeCallbacks(this);
                        myActivity.hideLoading();
                    } else
                        handler.post(this);
                }
            });
        }

    }

    private void LoadInformationByMan(Man man) {
        //todo view mode

        edtNumberPhone.setEnabled(false);
        edtAddress.setEnabled(false);
        raFemale.setEnabled(false);
        raMale.setEnabled(false);
        imageAvatar.setEnabled(false);

        icDate.setImageDrawable(DrawableUtils.getIconAwesome(getContext(), FontAwesome.Icon.faw_calendar, getResources().getColor(R.color.primary_color)));
        icUser.setImageDrawable(DrawableUtils.getIconAwesome(getContext(), FontAwesome.Icon.faw_user, getResources().getColor(R.color.primary_color)));
        icEmail.setImageDrawable(DrawableUtils.getIconAwesome(getContext(), FontAwesome.Icon.faw_envelope, getResources().getColor(R.color.primary_color)));
        icJob.setImageDrawable(DrawableUtils.getIconAwesome(getContext(), FontAwesome.Icon.faw_trophy, getResources().getColor(R.color.primary_color)));

        if (!man.getImage().isEmpty())
            imageAvatar.setImageBitmap(BitmapUtils.getCircleBitmap(BitmapUtils.decodeBase64(man.getImage())));
        else
            imageAvatar.setImageDrawable(getResources().getDrawable(R.drawable.gjobsjpg));

        txtAccountName.setText(GlobalConstant.account.getStringTypeAccount());
        txtEmail.setText(man.getEmail());
        txtName.setText(man.getNameMan());
        edtAddress.setText(man.getAddress());
        txtDoB.setText(Utils.dateToString(man.getDob(), "dd/MM/yyyy"));
        edtNumberPhone.setText(man.getNumberphone());
        if (man.getGender())
            raMale.setChecked(true);
        else
            raFemale.setChecked(true);

        List<String> provinces = Arrays.asList(getContext().getResources().getStringArray(R.array.arrProvince));
        txtProvince.setText(provinces.get(man.getIdProvince()));


        settingByType();

    }

    private void settingByType() {
        if (TYPE_DETAIL.equals(getResources().getString(R.string.TYPE_DETAIL_USER_ADMIN)))
            settingByAdmin();
        else if (TYPE_DETAIL.equals(getResources().getString(R.string.TYPE_DETAIL_USER_APPLY)))
            settingByReciut();
    }

    private void settingByReciut() {
        btnApply.setTag(0);
        List<String> modes = Arrays.asList(getResources().getStringArray(R.array.arrModeContact));
        dialog = new MaterialDialog.Builder(getContext())
                .title(R.string.contact)
                .titleColorRes(R.color.primary_color)
                .items(modes)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        return true;
                    }
                })
                .titleColorRes(R.color.primary_color)
                .positiveText(R.string.accept)
                .positiveColorRes(R.color.primary_color)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //todo cập nhật trạng thái của user
                        int index = dialog.getSelectedIndex();
                        if(index == 0){
                            //todo mail
                            sendMailTo(myMan.getEmail());
                        }
                        else{
                            phoneTo(myMan.getNumberphone());
                        }
                    }
                })
                .build();

        dialog.setSelectedIndex(Integer.parseInt(btnApply.getTag().toString()));
        btnApply.setText(modes.get(0));


        btnApply.setVisibility(View.VISIBLE);
        btnApply.setText(getResources().getString(R.string.contact));
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo quản lý bài đăng
                int index = (int) btnApply.getTag();
                dialog.setSelectedIndex(index);
                dialog.show();
            }
        });
    }

    private void phoneTo(String numberphone) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numberphone));
        startActivity(intent);
    }

    private void sendMailTo(String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    private void settingByAdmin() {
        final FirebaseAccount fireAccount = new FirebaseAccount(getContext());
        fireAccount.findAccountByID(myMan.getIdMan());

        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(fireAccount.isComplete()){
                    if(fireAccount.isSuccessfully()){
                        myAccount = (Account)fireAccount.getData();
                        settingCheckPostDialog();

                    }
                    handler.removeCallbacks(this);
                }
                else
                    handler.post(this);
            }
        });

        btnApply.setVisibility(View.VISIBLE);
        btnApply.setText(getResources().getString(R.string.manageuser));
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo quản lý bài đăng
                int index = (int) btnApply.getTag();
                dialog.setSelectedIndex(index);
                dialog.show();
            }
        });
    }

    private void settingCheckPostDialog() {
        btnApply.setTag(myAccount.getStatus());
        List<String> statuses = Arrays.asList(getResources().getStringArray(R.array.arrStatusUser));
        dialog = new MaterialDialog.Builder(getContext())
                .title(R.string.manageuser)
                .titleColorRes(R.color.primary_color)
                .items(statuses)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        return true;
                    }
                })
                .titleColorRes(R.color.primary_color)
                .positiveText(R.string.accept)
                .positiveColorRes(R.color.primary_color)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //todo cập nhật trạng thái của user
                        changeStatusUserByIndex(dialog.getSelectedIndex());
                    }
                })
                .build();

        dialog.setSelectedIndex(Integer.parseInt(btnApply.getTag().toString()));
        btnApply.setText(statuses.get(myAccount.getStatus()));
    }

    private void changeStatusUserByIndex(final int selectedIndex) {
        if (myAccount != null) {
            final FirebaseAccount fireAccount = new FirebaseAccount(getContext());
            fireAccount.updateAccount(myAccount, FirebaseResponse.QUERY_STATUSPOST);
            final Handler hadler = new Handler();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (fireAccount.isComplete()) {
                        if (fireAccount.isSuccessfully()) {
                            myAccount = (Account) fireAccount.getData();
                            btnApply.setTag(selectedIndex);
                        }
                        Toast.makeText(getContext(), fireAccount.getMessageResult(), Toast.LENGTH_SHORT).show();
                        myActivity.hideLoading();
                        hadler.removeCallbacks(this);
                    } else
                        hadler.post(this);
                }
            };
            myActivity.showLoading();
            hadler.post(runnable);
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
