package hufi.gjobs.Views.Map;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Created by baobao1996mn on 26/11/2017.
 */

public class FindLatLong extends AsyncTask<Void, Void, String> {
    String place;
    Context context;
    public LatLng point;
    public boolean isComplete;
    public boolean isSuccessfully;


    public LatLng getPoint() {
        return point;
    }

    public FindLatLong(Context context, String place) {
        super();
        this.place = place;
        this.context = context;
        this.isComplete = false;
        this.isSuccessfully = false;
    }

    @Override
    protected void onCancelled() {
        // TODO Auto-generated method stub
        super.onCancelled();
        this.cancel(true);
    }

    @Override
    protected String doInBackground(Void... params) {
        // TODO Auto-generated method stub
        try {
            StringBuilder jsonResults = new StringBuilder();
            String googleMapUrl = "http://maps.googleapis.com/maps/api/geocode/json?address="
                    + this.place.replaceAll(" ", "\\+") + "&sensor=true";
            return googleMapUrl;

        } catch (Exception e) {
            e.printStackTrace();
            Log.i("BaoSoCiu", e.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);

        final LoadDocumentTask load = new LoadDocumentTask(result);
        load.execute();
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (load.isCompleted) {
                    GetInfomation(load.doc);
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        });

    }
    private void GetInfomation(Document doc) {
        try {
            String str = doc.body().html();
            JSONObject jsonObj = new JSONObject(str);
            JSONArray resultJsonArray = jsonObj.getJSONArray("results");

            JSONObject before_geometry_jsonObj = resultJsonArray
                    .getJSONObject(0);

            JSONObject geometry_jsonObj = before_geometry_jsonObj
                    .getJSONObject("geometry");

            JSONObject location_jsonObj = geometry_jsonObj
                    .getJSONObject("location");

            String lat_helper = location_jsonObj.getString("lat");
            double lat = Double.valueOf(lat_helper);


            String lng_helper = location_jsonObj.getString("lng");
            double lng = Double.valueOf(lng_helper);

            point = new LatLng(lat, lng);

            isSuccessfully = true;

            Bundle bundle = new Bundle();
            bundle.putDouble("LATITUDE", lat);
            bundle.putDouble("LONGITUDE", lng);
            bundle.putString("LOCATION", place);

            Intent i = new Intent(context, MapsActivity.class);
            i.putExtra("BUNDLE", bundle);
            context.startActivity(i);
        } catch (Exception e) {
            Log.i("BaoSoCiu", e.getMessage());
            //Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            isSuccessfully = false;
        }
    }


    //TODO get data from internet
    private class LoadDocumentTask extends AsyncTask<Void, Void, String> {
        String href;
        Boolean isCompleted;
        public Document doc;

        public LoadDocumentTask(String href) {
            this.href = href;
            this.isCompleted = false;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                doc = Jsoup.connect(href)
                        .header("Accept-Encoding", "gzip, deflate")
                        .userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0")
                        .maxBodySize(0)
                        .timeout(600000)
                        .ignoreContentType(true)
                        .get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            isCompleted = true;
        }
    }
}
