package hufi.gjobs.Views.Profile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.philliphsu.bottomsheetpickers.date.DatePickerDialog;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hufi.gjobs.Master.GJobsActivity;
import hufi.gjobs.Master.GJobsFragment;
import hufi.gjobs.Master.GlobalConstant;
import hufi.gjobs.Model.Man;
import hufi.gjobs.R;
import hufi.gjobs.Service.FirebaseMan;
import hufi.gjobs.Service.FirebaseResponse;
import hufi.gjobs.Utils.BitmapUtils;
import hufi.gjobs.Utils.CircleImageView;
import hufi.gjobs.Utils.DrawableUtils;
import hufi.gjobs.Utils.Go;
import hufi.gjobs.Utils.ImagePicker;
import hufi.gjobs.Utils.Log;
import hufi.gjobs.Utils.Utils;
import hufi.gjobs.Views.ChangePassword.ChangepassFragment;

import static android.app.Activity.RESULT_OK;
import static hufi.gjobs.Master.GlobalConstant.man;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends GJobsFragment implements DatePickerDialog.OnDateSetListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.txtEmailUserInfo)
    public TextView txtEmail;
    @BindView(R.id.txtNameUserInfo)
    public TextView txtName;
    @BindView(R.id.accountNameUserInfo)
    public TextView txtAccountName;
    @BindView(R.id.txtDateofBirthUserInfo)
    public TextView txtDoB;
    @BindView(R.id.edtNumberphoneUserInfo)
    public EditText edtNumberPhone;
    @BindView(R.id.edtAddressUserInfo)
    public EditText edtAddress;
    @BindView(R.id.radioMaleUserInfo)
    public RadioButton raMale;
    @BindView(R.id.radioFemaleUserInfo)
    public RadioButton raFemale;
    @BindView(R.id.icDateOfBirthUserInfo)
    public ImageView icDate;
    @BindView(R.id.icNameUserInfo)
    public ImageView icUser;
    @BindView(R.id.icJobUserInfo)
    public ImageView icJob;
    @BindView(R.id.icEmailUserInfo)
    public ImageView icEmail;
    @BindView(R.id.imageUserInfo)
    public ImageView imageAvatar;
    @BindView(R.id.btnChangePass)
    public Button btnChangePass;
    @BindView(R.id.txtProvince)
    public TextView txtProvince;
    public static final int PICK_PHOTO_FOR_AVATAR = 1423;
    public static final int AVATAR_MAX_SIZE = 512;
    private static String encodedImage = "";
    MaterialDialog provinceDialog;
    DatePickerDialog datePicker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, v);
        myActivity = ((GJobsActivity)this.getActivity());
        setHasOptionsMenu(true);
        LoadInformation();

        return v;
    }

    //TODO menu


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_information, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_save_menu_information) {
            SaveInformation();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btnChangePass) public void  actionChangePass(){
        ChangepassFragment changePassFragment = new ChangepassFragment();
        Bundle bundle = new Bundle();
        bundle.putString("mTitle",getResources().getString(R.string.changepass));
        bundle.putBoolean("mArrow",true);
        changePassFragment.setArguments(bundle);
        Go.Add((AppCompatActivity)getContext(),changePassFragment,R.string.changepass);
    }


    private void LoadInformation() {
        icDate.setImageDrawable(DrawableUtils.getIconAwesome(getContext(), FontAwesome.Icon.faw_calendar, getResources().getColor(R.color.primary_color)));
        icUser.setImageDrawable(DrawableUtils.getIconAwesome(getContext(), FontAwesome.Icon.faw_user, getResources().getColor(R.color.primary_color)));
        icEmail.setImageDrawable(DrawableUtils.getIconAwesome(getContext(), FontAwesome.Icon.faw_envelope, getResources().getColor(R.color.primary_color)));
        icJob.setImageDrawable(DrawableUtils.getIconAwesome(getContext(), FontAwesome.Icon.faw_trophy, getResources().getColor(R.color.primary_color)));

        if (!man.getImage().isEmpty())
            imageAvatar.setImageBitmap(BitmapUtils.getCircleBitmap(BitmapUtils.decodeBase64(man.getImage())));
        else
            imageAvatar.setImageDrawable(getResources().getDrawable(R.drawable.gjobsjpg));

        txtAccountName.setText(GlobalConstant.account.getStringTypeAccount());
        txtEmail.setText(man.getEmail());
        txtName.setText(man.getNameMan());
        edtAddress.setText(man.getAddress());
        txtDoB.setText(Utils.dateToString(man.getDob(), "dd/MM/yyyy"));
        edtNumberPhone.setText(man.getNumberphone());
        if (man.getGender())
            raMale.setChecked(true);
        else
            raFemale.setChecked(true);

        settingDatePicker();
        settingPicturePicker();
        settingProvinceDialog();
    }

    @OnClick(R.id.txtProvince)
    public void showProvince() {
        //LẤY tag để thay thế getSelectedIndex
        int index = (int)txtProvince.getTag();
        provinceDialog.setSelectedIndex(index);
        provinceDialog.show();
    }

    public void settingProvinceDialog() {
        txtProvince.setTag(man.getIdProvince());

        List<String> provinces = Arrays.asList(getResources().getStringArray(R.array.arrProvince));
        provinceDialog = new MaterialDialog.Builder(getContext())
                .title(R.string.province)
                .titleColorRes(R.color.primary_color)
                .items(provinces)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        return true;
                    }
                })
                .titleColorRes(R.color.primary_color)
                .positiveText(R.string.choose)
                .positiveColorRes(R.color.primary_color)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        String str = dialog.getItems().get(dialog.getSelectedIndex()).toString();
                        txtProvince.setText(str);
                        txtProvince.setTag(dialog.getSelectedIndex());
                    }
                })
                .build();
        provinceDialog.setSelectedIndex(Integer.parseInt(txtProvince.getTag().toString()));

        txtProvince.setText(provinceDialog.getItems().get(man.getIdProvince()).toString());
    }

    private void settingPicturePicker() {
        imageAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(getActivity());
                startActivityForResult(chooseImageIntent, PICK_PHOTO_FOR_AVATAR);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == RESULT_OK) {
            Bitmap bitmap = ImagePicker.getImageFromResult(getActivity(), resultCode, data);
            bitmap = BitmapUtils.getResizedBitmap(bitmap, AVATAR_MAX_SIZE);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            Glide.with(this).load(b).thumbnail(0.5f).transform(new CircleImageView(getActivity())).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageAvatar);
            Log.i(encodedImage);
        }
    }

    private void settingDatePicker() {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int day = now.get(Calendar.DAY_OF_MONTH);

        if (man.getDob() != null) {
            year = man.getDob().getYear();
            month = man.getDob().getMonth();
            day = man.getDob().getDay();
        }

        datePicker = new DatePickerDialog.Builder(this, year, month, day).build();
        datePicker.setAccentColor(getResources().getColor(R.color.primary_color));
        txtDoB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker.show(getFragmentManager(), getResources().getString(R.string.item_menu_profile));
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void SaveInformation() {
        Date dob = Utils.stringToDate(txtDoB.getText().toString(), "dd/MM/yyyy");
        String phonenumber = edtNumberPhone.getText().toString();
        String address = edtAddress.getText().toString();
        int positionProvince = Integer.parseInt(txtProvince.getTag().toString());
        boolean gender = raMale.isChecked();

        boolean check = check(dob, phonenumber, address, positionProvince, gender);
        if (!check)
            return;

        //cập nhật
        final FirebaseMan firebaseMan = new FirebaseMan(getContext(), GlobalConstant.account.getTypeAccount());
        Man updatedMan = new Man(GlobalConstant.man);
        updatedMan.setDob(dob);
        updatedMan.setNumberphone(phonenumber);
        updatedMan.setAddress(address);
        updatedMan.setIdProvince(positionProvince);
        updatedMan.setGender(gender);

        if(encodedImage != null && !encodedImage.isEmpty())
            updatedMan.setImage(encodedImage);

        firebaseMan.updateMan(updatedMan, FirebaseResponse.QUERY_UPDATE_MAN);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (firebaseMan.isComplete()) {
                    Toast.makeText(getContext(), firebaseMan.getMessageResult(), Toast.LENGTH_SHORT).show();
                    if (firebaseMan.isSuccessfully()) {
                        GlobalConstant.man = (Man) firebaseMan.getData();
                        GlobalConstant.updateHeaderMenu(getContext());
                    }
                    myActivity.hideLoading();
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        myActivity.showLoading();
        handler.post(runnable);
    }

    private boolean check(Date dob, String phonenumber, String address, int positionProvince, boolean gender) {
        if (dob == null || phonenumber.isEmpty() || address.isEmpty()) {
            Toast.makeText(getContext(), getResources().getString(R.string.UpdateMan_InputWrong), Toast.LENGTH_LONG).show();
            if (dob == null)
                txtDoB.setError(getResources().getString(R.string.Information_Require));
            else if (phonenumber.isEmpty())
                edtNumberPhone.setError(getResources().getString(R.string.Information_Require));
            else
                edtAddress.setError(getResources().getString(R.string.Information_Require));
            return false;
        }
        Man man = GlobalConstant.man;
        if ( ((man.getDob() != null && man.getDob().equals(dob)) || (man.getDob() == null))
                && encodedImage.isEmpty()
                && man.getNumberphone().equals(phonenumber) && man.getAddress().equals(address)
                && man.getIdProvince() == positionProvince && (man.getGender() == gender)) {
            Toast.makeText(getContext(), getResources().getString(R.string.UpdateMan_NotChange), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        Calendar cal = new java.util.GregorianCalendar();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        txtDoB.setText(Utils.dateToString(cal.getTime(), "dd/MM/yyyy"));
    }
}
