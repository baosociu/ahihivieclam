package hufi.gjobs.Views.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hufi.gjobs.Master.GlobalConstant;
import hufi.gjobs.Model.Account;
import hufi.gjobs.Model.Man;
import hufi.gjobs.Model.Post;
import hufi.gjobs.Model.Report;
import hufi.gjobs.R;
import hufi.gjobs.Service.FirebaseAccount;
import hufi.gjobs.Service.FirebaseMan;
import hufi.gjobs.Utils.BitmapUtils;
import hufi.gjobs.Utils.DrawableUtils;
import hufi.gjobs.Utils.Go;
import hufi.gjobs.Utils.Utils;
import hufi.gjobs.Views.Detail.DetailPostFragment;
import hufi.gjobs.Views.Post.PostFragment;
import hufi.gjobs.Views.Report.ReportFragment;

import static hufi.gjobs.Master.GlobalConstant.COUNT_MIN;

/**
 * Created by baobao1996mn on 12/11/2017.
 */

public class RecyclerViewPostAdapter extends RecyclerView.Adapter<RecyclerViewPostAdapter.PostViewHolder> {
    List<PostViewHolder> listViewHolder;
    List<String> provinces;
    List<String> topics;
    Context context;
    Fragment fragment;
    List<Post> data;
    List<Report> dataReport;
    int count = 0;
    boolean isCompleted;
    //int countReport = 0;


    public RecyclerViewPostAdapter(Fragment fragment, Object list) {
        this.context = fragment.getContext();
        this.fragment = fragment;
        if(fragment instanceof ReportFragment)
        {
            this.data = (ArrayList<Post>)((Object[])list)[0];
            this.dataReport = (ArrayList<Report>)((Object[])list)[1];
        }
        else
        {
            this.data = (ArrayList<Post>)list;
            this.dataReport = new ArrayList<>();
        }

        provinces = Arrays.asList(context.getResources().getStringArray(R.array.arrProvince));
        topics= Arrays.asList(context.getResources().getStringArray(R.array.arrTopic));
        listViewHolder = new ArrayList<>();
    }



    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post_layout, null, false);
        PostViewHolder viewHolder = new PostViewHolder(v);
        if(!isCompleted)
            v.setVisibility(View.INVISIBLE);
        listViewHolder.add(viewHolder);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final PostViewHolder holder, int position) {
        final Post post = data.get(position);

        holder.txtTitle.setText(post.getTitlePost());
        holder.txtAddress.setText(provinces.get(post.getIdProvince()));
        holder.txtDate.setText(Utils.dateToString(post.getDateEnd(), "dd/MM/yyyy"));
        holder.txtTopic.setText(topics.get(post.getIdTopic()));
        holder.iconRecuit.setImageDrawable(DrawableUtils.getIconAwesome(context, FontAwesome.Icon.faw_user,context.getResources().getColor(R.color.primary_color)));
        holder.iconTopic.setImageDrawable(DrawableUtils.getIconAwesome(context, FontAwesome.Icon.faw_folder_open,context.getResources().getColor(R.color.primary_color)));


        settingStatusPost(holder.viewStatus,holder.txtStatus,post.getStatusPost());

        //todo nếu fragemnt là báo cáo
        if(fragment instanceof ReportFragment){
            settingInforReport(holder,position);
        }

        //todo lấy thông tin của nhà tuyển dụng
        final FirebaseMan firebaseMan = new FirebaseMan(context, Account.TYPE_BOSS);
        firebaseMan.findManByKey(post.getIdReciut());

        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (firebaseMan.isComplete()) {
                    if (firebaseMan.isSuccessfully()) {
                        count++;

                        //todo label
                        holder.lblOutDate.setText(R.string.deadline);
                        holder.lblReasonReport.setText(R.string.reportreason
                        );
                        holder.lblReportBy.setText(R.string.reportby);

                        Man boss = (Man) firebaseMan.getData();
                        holder.txtRecuit.setText(boss.getNameMan());
                        if (!boss.getImage().isEmpty())
                            holder.imagePost.setImageBitmap(BitmapUtils.getCircleBitmap(BitmapUtils.decodeBase64(boss.getImage())));
                        else
                            holder.imagePost.setImageDrawable(context.getResources().getDrawable(R.drawable.gjobsjpg));
                    }
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo: Type_post: nhà tuyển dụng thêm bài đăng mới
                DetailPostFragment detailPostFragment = new DetailPostFragment();
                Bundle bundle = new Bundle();

                int idType = -1;

                if(GlobalConstant.account.getTypeAccount() == Account.TYPE_ADMIN)
                    idType = R.string.TYPE_POST_ADMIN_DETAIL;
                else if(GlobalConstant.account.getTypeAccount() == Account.TYPE_BOSS)
                {
                    if(fragment instanceof PostFragment)
                        idType = R.string.TYPE_POST_RECUIT_EDIT; //bài của nahf tuyển dụng
                    else
                        idType = R.string.TYPE_POST_RECUIT_DETAIL;
                }
                else if(GlobalConstant.account.getTypeAccount() == Account.TYPE_CANDIDATE)
                    idType = R.string.TYPE_POST_USER;

                if(idType !=-1)
                    bundle.putString("TYPE_POST",context.getResources().getString(idType));

                bundle.putString("ID_POST",post.getIdPost());
                bundle.putString("ID_RECIUT",post.getIdReciut());
                bundle.putString("mTitle",context.getResources().getString(R.string.item_detail_post));
                bundle.putBoolean("mArrow",true);
                detailPostFragment.setArguments(bundle);
                Go.Add((AppCompatActivity)context,detailPostFragment,R.string.item_detail_post);
            }
        });
    }

    private void settingInforReport(final PostViewHolder holder, int position) {
        holder.lblReasonReport.setVisibility(View.VISIBLE);
        holder.lblReportBy.setVisibility(View.VISIBLE);
        holder.txtReasonReport.setVisibility(View.VISIBLE);
        holder.txtReportBy.setVisibility(View.VISIBLE);

        Report r = dataReport.get(position);
        holder.txtReasonReport.setText(r.getReason());

        final FirebaseAccount fireAccount = new FirebaseAccount(context);
        fireAccount.findAccountByID(r.getIdReportman());

        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(fireAccount.isComplete()){
                    if(fireAccount.isSuccessfully()){
                        //countReport++;

                        holder.itemView.setVisibility(View.VISIBLE);

                        Account a = (Account)fireAccount.getData();
                        holder.txtReportBy.setText(a.getUsername());
                    }
                    handler.removeCallbacks(this);
                }
                else
                    handler.post(this);
            }
        });
    }

    public boolean isCompleted(){
        isCompleted = count == data.size() || count >= COUNT_MIN;
        if(isCompleted)
            for (PostViewHolder p : listViewHolder)
                p.itemView.setVisibility(View.VISIBLE);
        return isCompleted;
    }

    private void settingStatusPost(View viewStatus, TextView txtStatus, int status) {
        if (status == Post.STATUS_POST_WAITING) {
            viewStatus.setBackgroundColor(context.getResources().getColor(R.color.color_waiting_status));
            txtStatus.setText(context.getResources().getString(R.string.StatusWaitingPost));
        } else if (status == Post.STATUS_POST_APPROVED) {
            viewStatus.setBackgroundColor(context.getResources().getColor(R.color.color_approved_status));
            txtStatus.setText(context.getResources().getString(R.string.StatusApprovedPost));
        } else if (status == Post.STATUS_POST_BLOCKED) {
            viewStatus.setBackgroundColor(context.getResources().getColor(R.color.color_blocked_status));
            txtStatus.setText(context.getResources().getString(R.string.StatusBlockedPost));
        } else {
            viewStatus.setBackgroundColor(context.getResources().getColor(R.color.color_blocked_status));
            txtStatus.setText(context.getResources().getString(R.string.StatusClosedPost));
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    protected class PostViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_post_address)
        TextView txtAddress;
        @BindView(R.id.item_post_date)
        TextView txtDate;
        @BindView(R.id.item_post_recruit)
        TextView txtRecuit;
        @BindView(R.id.item_post_title)
        TextView txtTitle;
        @BindView(R.id.item_post_image)
        ImageView imagePost;
        @BindView(R.id.item_post_status)
        TextView txtStatus;
        @BindView(R.id.item_post_view_status)
        View viewStatus;
        @BindView(R.id.item_post_topic_icon)
        ImageView iconTopic;
        @BindView(R.id.item_post_recuit_icon)
        ImageView iconRecuit;
        @BindView(R.id.item_post_topic)
        TextView txtTopic;
        @BindView(R.id.item_post_reportby)
        TextView txtReportBy;
        @BindView(R.id.item_post_reasonreport)
        TextView txtReasonReport;
        @BindView(R.id.item_post_lbl_reportby)
        TextView lblReportBy;
        @BindView(R.id.item_post_lbl_reasonreport)
        TextView lblReasonReport;
        @BindView(R.id.item_post_lbl_outdate)
        TextView lblOutDate;


        public PostViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            lblReasonReport.setVisibility(View.GONE);
            lblReportBy.setVisibility(View.GONE);
            txtReasonReport.setVisibility(View.GONE);
            txtReportBy.setVisibility(View.GONE);
        }
    }
}
