package hufi.gjobs.Views.Detail;

import android.content.Intent;
import android.graphics.Color;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.philliphsu.bottomsheetpickers.date.DatePickerDialog;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hufi.gjobs.Master.GJobsFragment;
import hufi.gjobs.Model.Account;
import hufi.gjobs.Model.Apply;
import hufi.gjobs.Model.Man;
import hufi.gjobs.Model.Post;
import hufi.gjobs.Model.Report;
import hufi.gjobs.R;
import hufi.gjobs.Service.FirebaseAccount;
import hufi.gjobs.Service.FirebaseApply;
import hufi.gjobs.Service.FirebaseMan;
import hufi.gjobs.Service.FirebasePost;
import hufi.gjobs.Service.FirebaseReport;
import hufi.gjobs.Service.FirebaseResponse;
import hufi.gjobs.Utils.BitmapUtils;
import hufi.gjobs.Utils.DrawableUtils;
import hufi.gjobs.Utils.Log;
import hufi.gjobs.Utils.Utils;
import hufi.gjobs.Views.Map.FindLatLong;

import static hufi.gjobs.Master.GlobalConstant.account;
import static hufi.gjobs.Master.GlobalConstant.man;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetailPostFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetailPostFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailPostFragment extends GJobsFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public DetailPostFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailPostFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailPostFragment newInstance(String param1, String param2) {
        DetailPostFragment fragment = new DetailPostFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    String TYPE;
    String ID_POST;
    String ID_RECIUT;
    Post myPost;
    Man myReciut; //thông tin ủa người đăng pôst
    Account myAccount; //account của người đăng post

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            TYPE = getArguments().getString("TYPE_POST", "");
            ID_POST = getArguments().getString("ID_POST", "");
            ID_RECIUT = getArguments().getString("ID_RECIUT", "");
        }
    }

    @BindView(R.id.btnApply)
    public Button btnApply;

    @BindView(R.id.edtTitleDetailPost)
    public EditText edtTitle;
    @BindView(R.id.edtContentDetailPost)
    public EditText edtContent;

    @BindView(R.id.txtTopic)
    public TextView txtTopic;
    @BindView(R.id.txtNameDetailPost)
    public TextView txtName;
    @BindView(R.id.txtEmailDetailPost)
    public TextView txtEmail;
    @BindView(R.id.txtDateDetailPost)
    public TextView txtDatePost;
    @BindView(R.id.txtProvince)
    public TextView txtProvince;
    @BindView(R.id.txtDateStart)
    public TextView txtDateStart;
    @BindView(R.id.txtDateEnd)
    public TextView txtDateEnd;
    @BindView(R.id.typeAccountNameDetailPost)
    public TextView txtTypeAccount;

    @BindView(R.id.imageDetailPost)
    public ImageView imageAvatar;
    @BindView(R.id.btnReported)
    public Button btnReportedView;

    @BindView(R.id.icDateStart)
    public ImageView icDateStart;
    @BindView(R.id.icDateEnd)
    public ImageView icDateEnd;
    @BindView(R.id.icEmailDetailPost)
    public ImageView icMail;
    @BindView(R.id.icJobDetailPost)
    public ImageView icJob;
    @BindView(R.id.icNameDetailPost)
    public ImageView icUser;
    @BindView(R.id.viewStatusPost)
    public View viewStatus;
    @BindView(R.id.txtStatusPost)
    public TextView txtStatus;
    @BindView(R.id.edtAddress)
    public TextView edtAddress;
    @BindView(R.id.icMaps)
    public ImageView icMaps;
    @BindView(R.id.viewAddress)
    public View viewAddress;

    MaterialDialog provinceDialog;
    MaterialDialog topicDialog;
    MaterialDialog checkPostDialog;
    MaterialDialog dialog; //CONTACT
    DatePickerDialog dateStartPicker;
    DatePickerDialog dateEndPicker;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detail_post, container, false);
        ButterKnife.bind(this, v);
        setHasOptionsMenu(true);

        settingLayout();

        return v;
    }

    private void settingLayout() {
        //TODO picker - dialog
        icDateEnd.setImageDrawable(DrawableUtils.getIconAwesome(getContext(), FontAwesome.Icon.faw_calendar, getResources().getColor(R.color.primary_color)));
        icDateStart.setImageDrawable(DrawableUtils.getIconAwesome(getContext(), FontAwesome.Icon.faw_calendar, getResources().getColor(R.color.primary_color)));
        icUser.setImageDrawable(DrawableUtils.getIconAwesome(getContext(), FontAwesome.Icon.faw_user, getResources().getColor(R.color.primary_color)));
        icMail.setImageDrawable(DrawableUtils.getIconAwesome(getContext(), FontAwesome.Icon.faw_envelope, getResources().getColor(R.color.primary_color)));
        icJob.setImageDrawable(DrawableUtils.getIconAwesome(getContext(), FontAwesome.Icon.faw_trophy, getResources().getColor(R.color.primary_color)));
        icMaps.setImageDrawable(DrawableUtils.getIconAwesome(getContext(), FontAwesome.Icon.faw_map, getResources().getColor(R.color.primary_color)));


        settingProvinceDialog();
        settingTopicDialog();

        settingDateStartPicker();
        settingDateEndPicker();

        //todo setting infor recuit
        if (ID_RECIUT.isEmpty()) {
            ID_RECIUT = man.getIdMan();
        }

        final FirebaseMan firebaseMan = new FirebaseMan(getContext(), Account.TYPE_BOSS);
        firebaseMan.findManByKey(ID_RECIUT);
        final Handler handler = new Handler();
        myActivity.showLoading();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (firebaseMan.isComplete()) {
                    if (firebaseMan.isSuccessfully()) {
                        myReciut = (Man) firebaseMan.getData();
                        settingLayoutByReciut();

                        if (!ID_POST.isEmpty()) {
                            final FirebasePost firebasePost = new FirebasePost(getContext());
                            firebasePost.findPostByID(ID_POST);

                            final Handler handler2 = new Handler();
                            handler2.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (firebasePost.isComplete()) {
                                        if (firebasePost.isSuccessfully()) {
                                            myPost = (Post) firebasePost.getData();
                                            //todo cài đặt layout theo loại
                                            settingLayoutByType();
                                        } else {
                                            settingDefaultLayout();
                                            myActivity.hideLoading();
                                        }
                                        handler2.removeCallbacks(this);
                                    } else
                                        handler2.post(this);
                                }


                            });
                        } else {
                            settingDefaultLayout();
                            //todo cài đặt layout theo loại
                            settingLayoutForRecuitAdd();
                            myActivity.hideLoading();
                        }
                    } else
                        myActivity.hideLoading();
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        });


    }

    private void settingDefaultLayout() {
        txtDatePost.setText(Utils.dateStringNow("dd/MM/yyyy"));
        txtStatus.setVisibility(View.INVISIBLE);
        viewStatus.setVisibility(View.INVISIBLE);
        edtContent.setText("");
        edtTitle.setText("");
        edtAddress.setText("");
    }

    private void settingLayoutByReciut() {
        txtName.setText(myReciut.getNameMan());
        txtEmail.setText(myReciut.getEmail());

        if (!myReciut.getImage().isEmpty())
            imageAvatar.setImageBitmap(BitmapUtils.getCircleBitmap(BitmapUtils.decodeBase64(myReciut.getImage())));
        else
            imageAvatar.setImageDrawable(getResources().getDrawable(R.drawable.gjobsjpg));

        final FirebaseAccount fireAccount = new FirebaseAccount(getContext());
        fireAccount.findAccountByEmail(myReciut.getEmail());

        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (fireAccount.isComplete()) {
                    if (fireAccount.isSuccessfully()) {
                        myAccount = (Account) fireAccount.getData();
                        settingLayoutByAccount();
                    }

                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        });

    }

    private void settingLayoutByAccount() {
        txtTypeAccount.setText(myAccount.getStringTypeAccount());
    }


    private void settingCheckPostDialog() {
        btnApply.setTag(myPost.getStatusPost());
        List<String> statuses = Arrays.asList(getResources().getStringArray(R.array.arrStatusPost));
        checkPostDialog = new MaterialDialog.Builder(getContext())
                .title(R.string.checkpost)
                .titleColorRes(R.color.primary_color)
                .items(statuses)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        return true;
                    }
                })
                .titleColorRes(R.color.primary_color)
                .positiveText(R.string.choose)
                .positiveColorRes(R.color.primary_color)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //todo cập nhật trạng thái của post
                        changeStatusPostByIndex(dialog.getSelectedIndex());
                    }
                })
                .build();

        checkPostDialog.setSelectedIndex(Integer.parseInt(btnApply.getTag().toString()));
        btnApply.setText(statuses.get(myPost.getStatusPost()));
    }

    private void changeStatusPostByIndex(final int index) {
        if (myPost != null) {
            myPost.setStatusPost(index);
            final FirebasePost firePost = new FirebasePost(getContext());
            firePost.updatePost(myPost, FirebaseResponse.QUERY_STATUSPOST);
            final Handler hadler = new Handler();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (firePost.isComplete()) {
                        if (firePost.isSuccessfully()) {
                            myPost = (Post) firePost.getData();
                            btnApply.setTag(index);
                            settingStatusPost(myPost.getStatusPost());
                        }
                        Toast.makeText(getContext(), firePost.getMessageResult(), Toast.LENGTH_SHORT).show();
                        myActivity.hideLoading();
                        hadler.removeCallbacks(this);
                    } else
                        hadler.post(this);
                }
            };
            myActivity.showLoading();
            hadler.post(runnable);
        }
    }

    @OnClick(R.id.txtTopic)
    public void showTopic() {
        //LẤY tag để thay thế getSelectedIndex
        int index = (int) txtTopic.getTag();
        topicDialog.setSelectedIndex(index);
        topicDialog.show();
    }

    private void settingTopicDialog() {
        txtTopic.setTag(0);

        List<String> topics = Arrays.asList(getResources().getStringArray(R.array.arrTopic));
        topicDialog = new MaterialDialog.Builder(getContext())
                .title(R.string.topic)
                .titleColorRes(R.color.primary_color)
                .items(topics)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        return true;
                    }
                })
                .titleColorRes(R.color.primary_color)
                .positiveText(R.string.choose)
                .positiveColorRes(R.color.primary_color)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        String str = dialog.getItems().get(dialog.getSelectedIndex()).toString();
                        txtTopic.setText(str);
                        txtTopic.setTag(dialog.getSelectedIndex());
                    }
                })
                .build();
        topicDialog.setSelectedIndex(Integer.parseInt(txtTopic.getTag().toString()));

        txtTopic.setText(topics.get(0));
    }

    @OnClick(R.id.txtProvince)
    public void showProvince() {
        //LẤY tag để thay thế getSelectedIndex
        int index = (int) txtProvince.getTag();
        provinceDialog.setSelectedIndex(index);
        provinceDialog.show();
    }

    private void settingProvinceDialog() {
        txtProvince.setTag(man.getIdProvince());

        List<String> provinces = Arrays.asList(getResources().getStringArray(R.array.arrProvince));
        provinceDialog = new MaterialDialog.Builder(getContext())
                .title(R.string.province)
                .titleColorRes(R.color.primary_color)
                .items(provinces)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        return true;
                    }
                })
                .titleColorRes(R.color.primary_color)
                .positiveText(R.string.choose)
                .positiveColorRes(R.color.primary_color)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        String str = dialog.getItems().get(dialog.getSelectedIndex()).toString();
                        txtProvince.setText(str);
                        txtProvince.setTag(dialog.getSelectedIndex());
                    }
                })
                .build();
        provinceDialog.setSelectedIndex(Integer.parseInt(txtProvince.getTag().toString()));

        txtProvince.setText(provinceDialog.getItems().get(man.getIdProvince()).toString());
    }

    private void settingDateStartPicker() {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int day = now.get(Calendar.DAY_OF_MONTH);

        txtDateStart.setText(Utils.dateToString(now.getTime(), "dd/MM/yyyy"));

        dateStartPicker = new DatePickerDialog.Builder(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = new java.util.GregorianCalendar();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                txtDateStart.setText(Utils.dateToString(cal.getTime(), "dd/MM/yyyy"));
            }
        }, year, month, day).build();
        dateStartPicker.setAccentColor(getResources().getColor(R.color.primary_color));
        txtDateStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateStartPicker.show(getFragmentManager(), getResources().getString(R.string.item_detail_post));
            }
        });
    }

    private void settingDateEndPicker() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MONTH, 6);
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int day = now.get(Calendar.DAY_OF_MONTH);
        txtDateEnd.setText(Utils.dateToString(now.getTime(), "dd/MM/yyyy"));

        dateEndPicker = new DatePickerDialog.Builder(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = new java.util.GregorianCalendar();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                txtDateEnd.setText(Utils.dateToString(cal.getTime(), "dd/MM/yyyy"));
            }
        }, year, month, day).build();
        dateEndPicker.setAccentColor(getResources().getColor(R.color.primary_color));
        txtDateEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateEndPicker.show(getFragmentManager(), getResources().getString(R.string.item_detail_post));
            }
        });
    }


    private void settingLayoutByType() {
        if (TYPE.equals(getResources().getString(R.string.TYPE_POST_RECUIT_DETAIL)))
            settingLayoutForView(false);
        else if (TYPE.equals(getResources().getString(R.string.TYPE_POST_RECUIT_EDIT)))
            settingLayoutForRecuitEdit();
        else if (TYPE.equals(getResources().getString(R.string.TYPE_POST_USER)))
            settingLayoutForUser();
        else if (TYPE.equals(getResources().getString(R.string.TYPE_POST_ADMIN_DETAIL)))
            settingLayoutForAdmin();

    }


    private void settingLayoutForAdmin() {
        settingLayoutForView(false);
        myActivity.hideLoading();
        settingCheckPostDialog();
        btnApply.setVisibility(View.VISIBLE);
        btnApply.setText(getResources().getString(R.string.checkpost));

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo quản lý bài đăng
                int index = (int) btnApply.getTag();
                checkPostDialog.setSelectedIndex(index);
                checkPostDialog.show();
            }
        });
    }

    private void settingLayoutForUser() {
        settingLayoutForView(false);
        btnApply.setEnabled(true);
        btnApply.setVisibility(View.VISIBLE);

        //todo kiểm tra post này người tìm việc đã appy chưa ?
        if (account.isCandidate() && !ID_POST.isEmpty()) {
            final FirebaseApply firebaseApply = new FirebaseApply(getContext());
            firebaseApply.findApplyByKey(man.getIdMan(), ID_POST);
            final Handler handler = new Handler();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (firebaseApply.isComplete()) {
                        if (firebaseApply.isSuccessfully()) {
                            settingContactButton();
                        } else {
                            settingApplyPostButton();
                        }


//                        btnApply.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//
//                                int index = (int) btnApply.getTag();
//                                if (index != -1)
//                                    dialog.setSelectedIndex(index);
//                                dialog.show();
//                            }
//                        });
                        myActivity.hideLoading();
                        handler.removeCallbacks(this);
                    } else
                        handler.post(this);
                }
            };
            handler.post(runnable);


        }
    }

    private void settingApplyPostButton() {
        btnApply.setTag(-1);
        btnApply.setVisibility(View.VISIBLE);
        btnApply.setText(getResources().getString(R.string.apply));
        dialog = new MaterialDialog.Builder(getContext())
                .title(R.string.apply)
                .titleColorRes(R.color.primary_color)
                .content(R.string.ApplyConfirm)
                .negativeText(R.string.cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                })
                .positiveColorRes(R.color.primary_color)
                .positiveText(R.string.accept)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        AddApply();
                    }
                }).build();

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();

            }
        });
    }

    private void AddApply() {
        //todo thêm id post vào listpost
        if (account.isCandidate() && !ID_POST.isEmpty()) {
            Apply apply = new Apply(man.getIdMan(), myPost.getIdReciut(), ID_POST, Calendar.getInstance().getTime());
            final FirebaseApply firebaseApply = new FirebaseApply(getContext());
            firebaseApply.addApply(apply);
            final Handler handler = new Handler();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (firebaseApply.isComplete()) {
                        Toast.makeText(getContext(), firebaseApply.getMessageResult(), Toast.LENGTH_SHORT).show();
                        if (firebaseApply.isSuccessfully()) {
                            //todo countApply++
                            plusCountApplyPost();
                        }
                        myActivity.hideLoading();
                        handler.removeCallbacks(this);
                    } else
                        handler.post(this);
                }
            };
            myActivity.showLoading();
            handler.post(runnable);
        }
    }

    private void plusCountApplyPost() {
        if (myPost != null) {
            myPost.setCountApply(myPost.getCountApply() + 1);
            final FirebasePost firebasePost = new FirebasePost(getContext());
            firebasePost.updatePost(myPost, FirebaseResponse.QUERY_PLUS_APPLY_POST);
            final Handler handler = new Handler();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (firebasePost.isComplete()) {
                        if (firebasePost.isSuccessfully())
                            settingContactButton();
                        else
                            myPost.setCountApply(myPost.getCountApply() == 0 ? 0 : myPost.getCountApply() + 1);
                        handler.removeCallbacks(this);
                    } else
                        handler.post(this);
                }
            });
        }

    }

    private void settingContactButton() {
        btnApply.setVisibility(View.VISIBLE);
        this.btnApply.setText(getResources().getString(R.string.contact));

        btnApply.setTag(0);
        List<String> modes = Arrays.asList(getResources().getStringArray(R.array.arrModeContact));
        dialog = new MaterialDialog.Builder(getContext())
                .title(R.string.contact)
                .titleColorRes(R.color.primary_color)
                .items(modes)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        return true;
                    }
                })
                .titleColorRes(R.color.primary_color)
                .positiveText(R.string.accept)
                .positiveColorRes(R.color.primary_color)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //todo cập nhật trạng thái của user
                        int index = dialog.getSelectedIndex();
                        if (index == 0) {
                            //todo mail
                            sendMailTo(myReciut.getEmail());
                        } else {
                            phoneTo(myReciut.getNumberphone());
                        }
                    }
                })
                .build();

        dialog.setSelectedIndex(Integer.parseInt(btnApply.getTag().toString()));

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });
    }

    private void phoneTo(String numberphone) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numberphone));
        startActivity(intent);
    }

    private void sendMailTo(String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    private void settingLayoutForRecuitEdit() {
        settingLayoutForView(true);
        myActivity.hideLoading();
        viewAddress.setVisibility(View.GONE);

        btnApply.setVisibility(View.VISIBLE);
        btnApply.setText(getResources().getString(R.string.closepost));

        if (myPost.getStatusPost() == Post.STATUS_POST_CLOSED) {
            settingClosedPostButton();
        }
        //todo cập nhật thông báo hết nhu cầu tuyển dụng
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(getContext())
                        .title(R.string.closepost)
                        .titleColorRes(R.color.primary_color)
                        .content(R.string.ClosepostConfirm)
                        .negativeText(R.string.cancel)
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                            }
                        })
                        .positiveColorRes(R.color.primary_color)
                        .positiveText(R.string.accept)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                ClosePost();
                            }
                        }).build().show();
            }
        });

    }

    private void ClosePost() {
        final FirebasePost firePost = new FirebasePost(getContext());
        firePost.updatePost(ID_POST, FirebaseResponse.QUERY_CLOSETOPIC);
        final Handler hadler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (firePost.isComplete()) {
                    if (firePost.isSuccessfully()) {
                        myPost = (Post) firePost.getData();
                        settingClosedPostButton(); //đóng bài đăng
                        settingStatusPost(myPost.getStatusPost());
                    }
                    Toast.makeText(getContext(), firePost.getMessageResult(), Toast.LENGTH_SHORT).show();
                    myActivity.hideLoading();
                    hadler.removeCallbacks(this);
                } else
                    hadler.post(this);
            }
        };
        myActivity.showLoading();
        hadler.post(runnable);
    }

    private void settingClosedPostButton() {
        this.btnApply.setBackgroundColor(getResources().getColor(R.color.md_red_500));
        this.btnApply.setText(getResources().getString(R.string.Closedpost));
        this.btnApply.setEnabled(false);

    }

    private void settingLayoutForView(boolean editable) {
        btnApply.setVisibility(View.GONE);

        //todo readonly
        edtContent.setEnabled(editable);
        edtTitle.setEnabled(editable);
        edtAddress.setEnabled(editable);
        txtTopic.setEnabled(editable);
        txtProvince.setEnabled(editable);
        txtDateEnd.setEnabled(editable);
        txtDatePost.setEnabled(editable);
        txtDateStart.setEnabled(editable);
        btnReportedView.setEnabled(editable);
        btnReportedView.setVisibility(View.GONE);
        settingLayoutByPost();

    }

    private void settingLayoutByPost() {
        //todo report button
        final FirebaseReport fireReport = new FirebaseReport(getContext());
        fireReport.findReportyByKey(ID_POST, account.getIdAccount());
        final Handler hanlder = new Handler();
        hanlder.post(new Runnable() {
            @Override
            public void run() {
                if (fireReport.isComplete()) {
                    if (fireReport.isSuccessfully())
                        btnReportedView.setVisibility(View.VISIBLE);
                    myActivity.hideLoading();
                    hanlder.removeCallbacks(this);
                } else
                    hanlder.post(this);
            }
        });

        //todo province
        txtProvince.setTag(myPost.getIdProvince());
        provinceDialog.setSelectedIndex(Integer.parseInt(txtProvince.getTag().toString()));
        txtProvince.setText(provinceDialog.getItems().get(myPost.getIdProvince()).toString());

        //todo linhvuc
        txtTopic.setTag(myPost.getIdTopic());
        topicDialog.setSelectedIndex(Integer.parseInt(txtTopic.getTag().toString()));
        txtTopic.setText(topicDialog.getItems().get(myPost.getIdTopic()).toString());

        //todo date
        txtDateStart.setText(Utils.dateToString(myPost.getDateStart(), "dd/MM/yyyy"));
        txtDateEnd.setText(Utils.dateToString(myPost.getDateEnd(), "dd/MM/yyyy"));
        txtDatePost.setText(Utils.dateToString(myPost.getDatePost(), "dd/MM/yyyy"));

        //todo text
        edtTitle.setText(myPost.getTitlePost());
        edtAddress.setText(myPost.getAddress());
        edtContent.setText(myPost.getContentPost());

        txtStatus.setVisibility(View.VISIBLE);
        viewStatus.setVisibility(View.VISIBLE);
        settingStatusPost(myPost.getStatusPost());
    }

    private void settingLayoutForRecuitAdd() {
        btnApply.setVisibility(View.VISIBLE);
        btnApply.setText(getResources().getString(R.string.addpost));
        viewAddress.setVisibility(View.GONE);
        //thêm post
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = edtTitle.getText().toString();
                String content = edtContent.getText().toString();
                String address = edtAddress.getText().toString();
                if (title.isEmpty() || content.isEmpty() || address.isEmpty())
                    Toast.makeText(getContext(), getResources().getString(R.string.Complete_Require), Toast.LENGTH_SHORT).show();
                else {
                    int idTopic = (int) txtTopic.getTag();
                    int idProvince = (int) txtProvince.getTag();
                    String idRecuit = man.getIdMan();
                    Date dateStart = Utils.stringToDate(txtDateStart.getText().toString(), "dd/MM/yyyy");
                    Date dateEnd = Utils.stringToDate(txtDateEnd.getText().toString(), "dd/MM/yyyy");
                    Date datePost = Utils.stringToDate(txtDatePost.getText().toString(), "dd/MM/yyyy");

                    Post p = new Post(idTopic, idProvince, idRecuit, title, content, dateStart, dateEnd, datePost, address);

                    final FirebasePost firePost = new FirebasePost(getContext());
                    firePost.addPost(p);
                    final Handler hadler = new Handler();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (firePost.isComplete()) {
                                Toast.makeText(getContext(), firePost.getMessageResult(), Toast.LENGTH_SHORT).show();
                                if (firePost.isSuccessfully()) {
                                    myActivity.onBackPressed();
                                }
                                myActivity.hideLoading();
                                hadler.removeCallbacks(this);
                            } else
                                hadler.post(this);
                        }
                    };
                    myActivity.showLoading();
                    hadler.post(runnable);
                }
            }
        });
        myActivity.hideLoading();
    }

    private void settingStatusPost(int status) {
        if (status == Post.STATUS_POST_WAITING) {
            viewStatus.setBackgroundColor(getResources().getColor(R.color.color_waiting_status));
            txtStatus.setText(getResources().getString(R.string.StatusWaitingPost));
        } else if (status == Post.STATUS_POST_APPROVED) {
            viewStatus.setBackgroundColor(getResources().getColor(R.color.color_approved_status));
            txtStatus.setText(getResources().getString(R.string.StatusApprovedPost));
        } else if (status == Post.STATUS_POST_BLOCKED) {
            viewStatus.setBackgroundColor(getResources().getColor(R.color.color_blocked_status));
            txtStatus.setText(getResources().getString(R.string.StatusBlockedPost));
        } else {
            viewStatus.setBackgroundColor(getResources().getColor(R.color.color_blocked_status));
            txtStatus.setText(getResources().getString(R.string.StatusClosedPost));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_report_item) {
            //todo báo cáo bài viết
            //todo kiểm tra bài viết này đã được người dùng báo cáo chưa?
            if (account != null && !ID_POST.isEmpty()) {
                final FirebaseReport firebaseReport = new FirebaseReport(getContext());
                firebaseReport.findReportyByKey(ID_POST, account.getIdAccount());
                final Handler handler = new Handler();
                myActivity.showLoading();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (firebaseReport.isComplete()) {
                            myActivity.hideLoading();
                            if (firebaseReport.isSuccessfully()) //todo rút report
                                new MaterialDialog.Builder(getContext())
                                        .title(R.string.report)
                                        .titleColorRes(R.color.primary_color)
                                        .content(R.string.ReportedPost)
                                        .negativeText(R.string.cancel)
                                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                            }
                                        })
                                        .positiveColorRes(R.color.primary_color)
                                        .positiveText(R.string.accept)
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                RemoveReport(ID_POST, account.getIdAccount());
                                            }
                                        }).build().show();
                            else //todo thêm report
                            {
                                List<String> reports = Arrays.asList(getResources().getStringArray(R.array.arrReportPost));
                                MaterialDialog dialog = new MaterialDialog.Builder(getContext())
                                        .title(R.string.report)
                                        .titleColorRes(R.color.primary_color)
                                        .items(reports)
                                        .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                                            @Override
                                            public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                                return true;
                                            }
                                        })
                                        .titleColorRes(R.color.primary_color)
                                        .negativeText(R.string.cancel)
                                        .positiveText(R.string.accept)
                                        .positiveColorRes(R.color.primary_color)
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                AddReport(ID_POST, account.getIdAccount(), dialog.getSelectedIndex());
                                            }
                                        })
                                        .build();
                                dialog.setSelectedIndex(0);
                                dialog.show();
                            }
                        } else
                            handler.post(this);
                    }
                });

            }
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    private void AddReport(String id_post, String idAccount, int selectedIndex) {
        List<String> reports = Arrays.asList(getResources().getStringArray(R.array.arrReportPost));

        final FirebaseReport firebase = new FirebaseReport(getContext());
        Report r = new Report(id_post, idAccount, Calendar.getInstance().getTime(), reports.get(selectedIndex), selectedIndex);
        firebase.addReport(r);
        final Handler handler = new Handler();
        myActivity.showLoading();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (firebase.isComplete()) {
                    Toast.makeText(getContext(), firebase.getMessageResult(), Toast.LENGTH_SHORT).show();
                    if (firebase.isSuccessfully())
                        btnReportedView.setVisibility(View.VISIBLE);
                    myActivity.hideLoading();
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        });
    }

    private void RemoveReport(String id_post, String idAccount) {
        final FirebaseReport firebase = new FirebaseReport(getContext());
        firebase.removeReport(id_post, idAccount);
        final Handler handler = new Handler();
        myActivity.showLoading();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (firebase.isComplete()) {
                    Toast.makeText(getContext(), firebase.getMessageResult(), Toast.LENGTH_SHORT).show();
                    if (firebase.isSuccessfully())
                        btnReportedView.setVisibility(View.GONE);
                    myActivity.hideLoading();
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        });
    }

    @OnClick(R.id.viewAddress)
    public void clickAddress() {
        if (TYPE.equals(getResources().getString(R.string.TYPE_POST_RECUIT_DETAIL))
                || TYPE.equals(getResources().getString(R.string.TYPE_POST_USER))
                || TYPE.equals(getResources().getString(R.string.TYPE_POST_ADMIN_DETAIL))) {
            String location = edtAddress.getText().toString();
            showMaps(location);
        }
    }

    private void showMaps(String location) {
        Geocoder gc = new Geocoder(getContext(), Locale.getDefault());
        try {
//            List<Address> addresses = gc.getFromLocationName(location, 10);
//
//            if (addresses != null && addresses.size() != 0) {
//                Address address = addresses.get(0);
//                Bundle bundle = new Bundle();
//                bundle.putDouble("LATITUDE", address.getLatitude());
//                bundle.putDouble("LONGITUDE", address.getLongitude());
//                bundle.putString("LOCATION", location);
//
//                Intent i = new Intent(getContext(), MapsActivity.class);
//                i.putExtra("BUNDLE", bundle);
//                startActivity(i);
//            }
            FindLatLong f = new FindLatLong(getContext(),location);
            f.execute();

        } catch (Exception e) {
            Log.i("Maps: " + e.getMessage().toString());
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        if (TYPE.equals(getResources().getString(R.string.TYPE_POST_RECUIT_DETAIL)) || TYPE.equals(getResources().getString(R.string.TYPE_POST_USER))) {
            inflater.inflate(R.menu.menu_report, menu);
            menu.findItem(R.id.menu_report_item).setIcon(DrawableUtils.getIconAwesomeActionBar(getContext(), FontAwesome.Icon.faw_flag, Color.WHITE));
        }
        super.onCreateOptionsMenu(menu, inflater);
    }
}
