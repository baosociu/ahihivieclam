package hufi.gjobs.Views.Register;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hufi.gjobs.Master.GJobsActivity;
import hufi.gjobs.R;
import hufi.gjobs.Service.FirebaseAccount;

public class RegisterActivity extends GJobsActivity {
    @BindView(R.id.edtUsernameAccoutnRegister)
    public EditText edtUsername;
    @BindView(R.id.edtEmailAccoutnRegister)
    public EditText edtEmail;
    @BindView(R.id.edtPasswordRegister)
    public EditText edtPassword;
    @BindView(R.id.edtPasswordConfirmRegister)
    public EditText edtPasswordConfirm;
    @BindView(R.id.relativeLayoutRegister)
    public RelativeLayout relative;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        setLoading(relative);
    }

    @OnClick(R.id.btnCountinueRegister)
    public void CheckAccount() {
        final String username = edtUsername.getText().toString();
        final String email = edtEmail.getText().toString();
        final String pass = edtPassword.getText().toString();
        String pass2 = edtPasswordConfirm.getText().toString();
        if(username.contains(" "))
        {
            edtUsername.setError(getResources().getString(R.string.Information_Contains_Space));
            return;
        }

        if (username.isEmpty() || email.isEmpty() || pass.isEmpty() || pass2.isEmpty()) {
            if (username.isEmpty())
                edtUsername.setError(getResources().getString(R.string.Information_Require));
            if (email.isEmpty())
                edtEmail.setError(getResources().getString(R.string.Information_Require));
            if (pass.isEmpty())
                edtPassword.setError(getResources().getString(R.string.Information_Require));
            if(pass2.isEmpty())
                edtPasswordConfirm.setError(getResources().getString(R.string.Information_Require));
            return;
        }

        if (pass.length() <= 6) {
            Toast.makeText(this, getResources().getString(R.string.PassChange_Min_Length), Toast.LENGTH_SHORT).show();
            return;
        }

        if (!pass.equals(pass2)) {
            Toast.makeText(this, getResources().getString(R.string.password_confirm_wrong), Toast.LENGTH_SHORT).show();
            return;
        }


        CheckUsernameAndEmail(username,email, pass);
    }

    private void CheckUsernameAndEmail(final String username, final String email, final String pass) {
        final FirebaseAccount firebaseAccount = new FirebaseAccount(this);
        firebaseAccount.findAccountByUsernameAndEmail(username, email);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (firebaseAccount.isComplete()) {
                    if (firebaseAccount.isSuccessfully())
                    {
                        Toast.makeText(getBaseContext(), getResources().getString(R.string.Register_Duplication), Toast.LENGTH_SHORT).show();
                    }
                    else
                        GoToCountinue(username, email, pass);
                    hideLoading();
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        showLoading();

        handler.post(runnable);
    }

    private void GoToCountinue(String username, String email, String pass) {
        Intent i = new Intent(RegisterActivity.this, RegisterInformationActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString("mUsername",username);
        bundle.putString("mEmail",email);
        bundle.putString("mPassword",pass);
        i.putExtra("mBundle",bundle);
        //sendMailVerification(email);
        startActivity(i);


    }



    private void goResultActivity(String maXacNhan) {
        Intent intent   = new Intent(RegisterActivity.this,ConfirmActivity.class);
        intent.putExtra("codeSend",maXacNhan);
        startActivity(intent);
    }



    public String getMa() {
        Random rd = new Random();
        return Math.abs(rd.nextInt()%9999)+"" ;


    }
}
