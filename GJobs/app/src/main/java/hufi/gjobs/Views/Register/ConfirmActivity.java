package hufi.gjobs.Views.Register;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import hufi.gjobs.Master.GJobsActivity;
import hufi.gjobs.R;
import hufi.gjobs.Views.Login.LoginActivity;

public class ConfirmActivity extends GJobsActivity {

    EditText edtCode;
    Button btnRegister_Recived, btnRegister_Close;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);

        btnRegister_Recived = findViewById(R.id.btnRegister_Recived);
        btnRegister_Close= findViewById(R.id.btnRegister_Close);
        edtCode = findViewById(R.id.edtCode);
        btnRegister_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                startActivity(intent);
            }
        });
        btnRegister_Recived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = getIntent();
                String codeSend = intent.getStringExtra("codeSend");
                String code = edtCode.getText().toString();
                if(!code.isEmpty()) {
                    if(codeSend.equals(code))
                    //setResult(29,intent);
                      finish();
                    else
                        Toast.makeText(ConfirmActivity.this,"Mã xác nhận không đúng",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
