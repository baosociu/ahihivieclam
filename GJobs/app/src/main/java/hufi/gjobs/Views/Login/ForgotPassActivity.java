package hufi.gjobs.Views.Login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import hufi.gjobs.Master.GJobsActivity;
import hufi.gjobs.Master.GJobsFunction;
import hufi.gjobs.Model.Account;
import hufi.gjobs.R;
import hufi.gjobs.Service.FirebaseAccount;

public class ForgotPassActivity extends GJobsActivity {

    EditText edtNameUser, edtEmailForgot;
    Button btnConfirm;
    RelativeLayout relative;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        edtNameUser = (EditText)findViewById(R.id.edtNameUser);
        edtEmailForgot = (EditText)findViewById(R.id.edtEmailForgot);
        btnConfirm = (Button)findViewById(R.id.btnConfirm);
        relative = (RelativeLayout)findViewById(R.id.relativeLayoutForgot) ;
        setLoading(relative);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = edtNameUser.getText().toString();
                String email = edtEmailForgot.getText().toString();
                if(!checkValue(name,email))
                    return;

                TodoForgot(name,email);
            }
        });
    }

    private void TodoForgot(String name, String email) {
        showLoading();
        final FirebaseAccount firebaseAccount = new FirebaseAccount(this);
        firebaseAccount.findAccountByUsernameAndEmail_Forgotpass(name, email);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (firebaseAccount.isComplete()) {
                    if (firebaseAccount.isSuccessfully())
                    {
                        Account acc = (Account) firebaseAccount.getData();
                        hideLoading();
                        sendEmail(acc);
                        Toast.makeText(getBaseContext(),"Mật khẩu mặc định đã gửi vào Email đăng kí của bạn",Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        hideLoading();
                        Toast.makeText(getBaseContext(),"Email hoặc tên người dùng chưa đúng",Toast.LENGTH_LONG).show();

                    }
                    hideLoading();
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };

        handler.post(runnable);


    }
    private void sendEmail(Account acc) {
        String matkhau = GJobsFunction.getPassDefault();
        acc.setPassword(matkhau);
        final FirebaseAccount firebase = new FirebaseAccount(this);
        firebase.update(acc, FirebaseAccount.QUERY_CHANGPASS);
        showLoading();
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(firebase.isComplete()){
                    if(firebase.isSuccessfully())
                    {
                        Account newACC = (Account) firebase.getData();
                        GJobsFunction.sendMailForgot(ForgotPassActivity.this, newACC.getEmail(), newACC.getPassword(), newACC.getUsername());
                        //todo: quay về đăng nhập
                        Intent i = new Intent(ForgotPassActivity.this, LoginActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("myUsername",newACC.getUsername());
                        i.putExtra("myBundle",bundle);
                        startActivity(i);
                    }
                    handler.removeCallbacks(this);
                    hideLoading();
                }
                else
                    handler.post(this);
            }
        });
    }

    private boolean checkValue(String name, String email) {
        if (name.isEmpty()) {
            edtNameUser.setError("Thông tin không hợp lệ");
            return false;
        }
        if (name.isEmpty()) {
            edtEmailForgot.setError("Thông tin không hợp lệ");
            return false;
        }
        return true;
    }

}
