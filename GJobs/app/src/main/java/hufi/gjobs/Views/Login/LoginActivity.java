package hufi.gjobs.Views.Login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hufi.gjobs.Master.GJobsActivity;
import hufi.gjobs.Master.GJobsFunction;
import hufi.gjobs.Master.GlobalConstant;
import hufi.gjobs.Model.Account;
import hufi.gjobs.Model.Man;
import hufi.gjobs.R;
import hufi.gjobs.Service.FirebaseAccount;
import hufi.gjobs.Service.FirebaseMan;
import hufi.gjobs.Utils.SharedPreferencesUtils;
import hufi.gjobs.Views.Main.MainActivity;
import hufi.gjobs.Views.Register.RegisterActivity;

import static hufi.gjobs.Master.GlobalConstant.USER_ACCOUNT;

public class LoginActivity extends GJobsActivity {

    @BindView(R.id.edtEmailLogin)
    public EditText edtEmail;
    @BindView(R.id.edtPasswordLogin)

    public EditText edtPass;
    @BindView(R.id.relativeLayoutLogin)
    public RelativeLayout relative;

    private MaterialDialog activeDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setLoading(relative);

        Bundle _bundle = getIntent().getBundleExtra("myBundle");
        String username = "";
        String password = "";
        if (_bundle != null) {
            username = _bundle.getString("myUsername", "");
            password = _bundle.getString("myPassword", "");
        }

        if (username.isEmpty() || password.isEmpty())
            autoLogin();
        else {
            edtEmail.setText(username);
            edtPass.setText(password);
        }
    }

    private void showActiveDialog(final Account acc) {
        activeDialog = new MaterialDialog.Builder(this)
                .title(R.string.active_account_title)
                .titleColorRes(R.color.primary_color)
                .content(R.string.active_account_content)
                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_NUMBER)
                .input(R.string.active_account_hint, R.string.empty, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {

                    }
                })
                .neutralText(R.string.send_code)
                .onNeutral(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        sendActiveCode(acc);
                    }
                })
                .positiveColorRes(R.color.primary_color)
                .positiveText(R.string.confirm)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        String input_code = dialog.getInputEditText().getText().toString();
                        if (input_code.equals(acc.getActiveCode())) {
                            acc.setActiveCode(null);
                            acc.setStatus(Account.STATUS_ACTIVED);

                            final FirebaseAccount firebase = new FirebaseAccount(LoginActivity.this);
                            firebase.update(acc, FirebaseAccount.QUERY_ACTIVE);
                            final Handler handler = new Handler();
                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    if (firebase.isComplete()) {
                                        Toast.makeText(LoginActivity.this, firebase.getMessageResult(), Toast.LENGTH_SHORT).show();
                                        hideLoading();
                                        if (firebase.isSuccessfully())
                                            Login(acc.getUsername(), acc.getPassword());
                                        handler.removeCallbacks(this);
                                    } else
                                        handler.post(this);
                                }
                            };
                            showLoading();
                            handler.post(runnable);
                        } else
                            Toast.makeText(LoginActivity.this, getResources().getString(R.string.Active_Account_Wrong), Toast.LENGTH_SHORT).show();
                    }
                }).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @OnClick(R.id.txtRegisterAccount)
    public void goToRegisterActivity() {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    }

    @OnClick({R.id.btnLogin})
    public void clickLogin() {
        String username = edtEmail.getText().toString();
        String password = edtPass.getText().toString();
        if (username.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, getResources().getString(R.string.Information_wrong), Toast.LENGTH_SHORT).show();
            return;
        }
        //todo đăng nhập
        Login(username, password);
    }
    @OnClick({R.id.txtFogotPass})
    public void fogotPass() {
        Intent intent = new Intent(getBaseContext(), ForgotPassActivity.class);
        startActivity(intent);
    }
    private void autoLogin() {
        Object d = SharedPreferencesUtils.getValue(this, USER_ACCOUNT, Account.class);
        if (d == null)
            return;
        Account ac = (Account) d;
        if (!ac.getUsername().isEmpty() && !ac.getPassword().isEmpty()) {
            Login(ac.getUsername(), ac.getPassword());
        }

    }

    private void Login(String username, String password) {
        showLoading();

        final FirebaseAccount firebase = new FirebaseAccount(this);
        firebase.login(username, password);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (firebase.isComplete()) {
                    Toast.makeText(getBaseContext(), firebase.getMessageResult(), Toast.LENGTH_SHORT).show();
                    if (firebase.isSuccessfully()) {
                        Account acc = (Account) firebase.getData();
                        if (acc.getStatus() == Account.STATUS_ACTIVED)
                            GoMainActivity(acc);
                        else {
                            showDialogActiveAccount(acc);
                            hideLoading(); //kết thúc
                        }
                    } else {
                        SharedPreferencesUtils.setValue(LoginActivity.this, USER_ACCOUNT, ""); //xoá dữ liệu auto login
                        hideLoading(); //kết thúc
                    }
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        handler.post(runnable);

    }

    private void showDialogActiveAccount(Account acc) {
        // if(activeCode.isEmpty()) //cấp lại code
        if (acc.getActiveCode() != null)
            showActiveDialog(acc);
        else {
            sendActiveCode(acc);
        }
    }

    private void sendActiveCode(Account acc) {
        String ma = GJobsFunction.getActiveCode();
        acc.setActiveCode(ma);
        FirebaseAccount firebase = new FirebaseAccount(this);
        firebase.update(acc, FirebaseAccount.QUERY_SENDCODE);
        GJobsFunction.sendMailVerification(this, acc.getEmail(), ma, acc.getUsername());
    }

    void GoMainActivity(Account account) {
        GlobalConstant.account = account;
        //todo lấy thông tin người dùng
        final FirebaseMan firebaseMan = new FirebaseMan(this, account.getTypeAccount());
        firebaseMan.findManByKey(account.getIdAccount());

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (firebaseMan.isComplete()) {
                    if (firebaseMan.isSuccessfully()) {
                        //tồn tại thông tin người dùng
                        GlobalConstant.man = (Man) firebaseMan.getData();
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.Login_Information_Successfully, GlobalConstant.man.getNameMan()), Toast.LENGTH_SHORT).show();
                        hideLoading(); //kết thúc
                    } else {
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.Login_Information_Failure), Toast.LENGTH_SHORT).show();
                        hideLoading(); //kết thúc
                    }
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        handler.post(runnable);
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(this)
                .title(R.string.exit_application)
                .titleColorRes(R.color.primary_color)
                .content(R.string.exit_content)
                .negativeText(R.string.cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                })
                .positiveColorRes(R.color.primary_color)
                .positiveText(R.string.exit)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }).show();
    }

}
