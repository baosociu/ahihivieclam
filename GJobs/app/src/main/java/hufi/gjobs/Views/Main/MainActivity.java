package hufi.gjobs.Views.Main;

import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hufi.gjobs.Master.GJobsActivity;
import hufi.gjobs.Master.GJobsFragment;
import hufi.gjobs.Master.GlobalConstant;
import hufi.gjobs.Model.Account;
import hufi.gjobs.R;
import hufi.gjobs.Utils.DrawableUtils;
import hufi.gjobs.Utils.Go;
import hufi.gjobs.Utils.SharedPreferencesUtils;
import hufi.gjobs.Views.Apply.ApplyFragment;
import hufi.gjobs.Views.Candidate.CandidateFragment;
import hufi.gjobs.Views.Check.CheckFragment;
import hufi.gjobs.Views.FindWork.FindFragment;
import hufi.gjobs.Views.Home.HomeFragment;
import hufi.gjobs.Views.Hot.HotFragment;
import hufi.gjobs.Views.Notification.NotificationFragment;
import hufi.gjobs.Views.Post.PostFragment;
import hufi.gjobs.Views.Profile.ProfileFragment;
import hufi.gjobs.Views.Report.ReportFragment;
import hufi.gjobs.Views.Setting.SettingFragment;
import hufi.gjobs.Views.User.UserFragment;

import static hufi.gjobs.Master.GlobalConstant.USER_ACCOUNT;
import static hufi.gjobs.Master.GlobalConstant.account;
import static hufi.gjobs.Master.GlobalConstant.header;
import static hufi.gjobs.Master.GlobalConstant.renewAll;
import static hufi.gjobs.Master.GlobalConstant.updateHeaderMenu;
import static hufi.gjobs.Utils.SharedPreferencesUtils.setValue;

public class MainActivity extends GJobsActivity implements Drawer.OnDrawerItemClickListener,
        GJobsFragment.OnFragmentInteractionListener, Drawer.OnDrawerNavigationListener {
    //@BindView(R.id.toolbar)
    public Toolbar toolbar;
    MaterialDialog alert;
    boolean acceptLogout;
    @BindView(R.id.relativeLayoutMain)
    public RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setLoading(relativeLayout);
        onDrawMenu();
        intDialogLogout();
        saveLogin();
    }

    private void saveLogin() {
        setValue(this,USER_ACCOUNT,account);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //todo fake data
    }

    private void onDrawMenu() {
        PrimaryDrawerItem item1 = new PrimaryDrawerItem()
                .withIdentifier(R.integer.item_menu_home)
                .withName(R.string.item_menu_home)
                .withTag(R.string.item_menu_home)
                .withIcon(DrawableUtils.getIconAwesome(this, FontAwesome.Icon.faw_home, getResources()
                        .getColor(R.color.colorPrimary)));

        SecondaryDrawerItem item2 = new SecondaryDrawerItem()
                .withIdentifier(R.integer.item_menu_notification)
                .withName(R.string.item_menu_notification)
                .withTag(R.string.item_menu_notification)
                .withIcon(DrawableUtils.getIconAwesome(this, FontAwesome.Icon.faw_globe, getResources()
                        .getColor(R.color.colorPrimary)));

        SecondaryDrawerItem item3 = new SecondaryDrawerItem()
                .withIdentifier(getResources().getInteger(R.integer.item_menu_find))
                .withName(R.string.item_menu_find)
                .withTag(R.string.item_menu_find)
                .withIcon(DrawableUtils.getIconAwesome(this, FontAwesome.Icon.faw_search, getResources()
                        .getColor(R.color.colorPrimary)));

        SecondaryDrawerItem item4 = new SecondaryDrawerItem()
                .withIdentifier(R.integer.item_menu_hot)
                .withName(R.string.item_menu_hot)
                .withTag(R.string.item_menu_hot)
                .withIcon(DrawableUtils.getIconAwesome(this, FontAwesome.Icon.faw_flask, getResources()
                        .getColor(R.color.colorPrimary)));

        SecondaryDrawerItem item5 = new SecondaryDrawerItem()
                .withIdentifier(R.integer.item_menu_profile)
                .withName(R.string.item_menu_profile)
                .withTag(R.string.item_menu_profile)
                .withIcon(DrawableUtils.getIconAwesome(this, FontAwesome.Icon.faw_user_circle, getResources()
                        .getColor(R.color.colorPrimary)));

        SecondaryDrawerItem item6 = new SecondaryDrawerItem()
                .withIdentifier(R.integer.item_menu_apply)
                .withName(R.string.item_menu_apply)
                .withTag(R.string.item_menu_apply)
                .withIcon(DrawableUtils.getIconAwesome(this, FontAwesome.Icon.faw_handshake_o, getResources()
                        .getColor(R.color.colorPrimary)));

        SecondaryDrawerItem item7 = new SecondaryDrawerItem()
                .withIdentifier(R.integer.item_menu_post)
                .withName(R.string.item_menu_post)
                .withTag(R.string.item_menu_post)
                .withIcon(DrawableUtils.getIconAwesome(this, FontAwesome.Icon.faw_check, getResources()
                        .getColor(R.color.colorPrimary)));

        SecondaryDrawerItem item8 = new SecondaryDrawerItem()
                .withIdentifier(R.integer.item_menu_candidate)
                .withName(R.string.item_menu_candidate)
                .withTag(R.string.item_menu_candidate)
                .withIcon(DrawableUtils.getIconAwesome(this, FontAwesome.Icon.faw_hand_paper_o, getResources()
                        .getColor(R.color.colorPrimary)));


        SecondaryDrawerItem item9 = new SecondaryDrawerItem()
                .withIdentifier(R.integer.item_menu_check)
                .withName(R.string.item_menu_check)
                .withTag(R.string.item_menu_check)
                .withIcon(DrawableUtils.getIconAwesome(this, FontAwesome.Icon.faw_gavel, getResources()
                        .getColor(R.color.colorPrimary)));

        SecondaryDrawerItem item10 = new SecondaryDrawerItem()
                .withIdentifier(getResources().getInteger(R.integer.item_menu_users))
                .withName(R.string.item_menu_users)
                .withTag(R.string.item_menu_users)
                .withIcon(DrawableUtils.getIconAwesome(this, FontAwesome.Icon.faw_users, getResources()
                        .getColor(R.color.colorPrimary)));

        SecondaryDrawerItem item11 = new SecondaryDrawerItem()
                .withIdentifier(getResources().getInteger(R.integer.item_menu_report))
                .withName(R.string.item_menu_report)
                .withTag(R.string.item_menu_report)
                .withIcon(DrawableUtils.getIconAwesome(this, FontAwesome.Icon.faw_thumbs_down, getResources()
                        .getColor(R.color.colorPrimary)));

        SecondaryDrawerItem item12 = new SecondaryDrawerItem()
                .withIdentifier(R.integer.item_menu_setting)
                .withName(R.string.item_menu_setting)
                .withTag(R.string.item_menu_setting)
                .withIcon(DrawableUtils.getIconAwesome(this, FontAwesome.Icon.faw_cogs, getResources()
                        .getColor(R.color.colorPrimary)));

        SecondaryDrawerItem item13 = new SecondaryDrawerItem()
                .withIdentifier(getResources().getInteger(R.integer.item_menu_logout))
                .withName(R.string.item_menu_logout)
                .withTag(R.string.item_menu_logout)
                .withIcon(DrawableUtils.getIconAwesome(this, FontAwesome.Icon.faw_sign_out, getResources()
                        .getColor(R.color.colorPrimary)));

        header = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(new ColorDrawable(getResources().getColor(R.color.colorPrimary)))
                .withOnAccountHeaderSelectionViewClickListener(new AccountHeader.OnAccountHeaderSelectionViewClickListener() {
                    @Override
                    public boolean onClick(View view, IProfile profile) {
                        GoProfile();
                        return true;
                    }
                })
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
                        GoProfile();
                        return true;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        return false;
                    }
                })
                .build();

        updateHeaderMenu(this);

        if(GlobalConstant.account.getStringTypeAccount().equals(Account.ADMIN)){
            GlobalConstant.menu = new DrawerBuilder()
                    .withActivity(this)
                    .withToolbar(toolbar)
                    .withAccountHeader(header)
                    .addDrawerItems(
                            item1,
                            new DividerDrawerItem(),
                            //item2,
                            item4,
                            item5,
                            item9,
                            item10,
                            item11,
                            //item12,
                            new DividerDrawerItem(),
                            item13
                    )
                    .withOnDrawerItemClickListener(this)
                    .withOnDrawerNavigationListener(this)
                    .withSelectedItem(R.integer.item_menu_home)
                    .build();
        }
        else if(GlobalConstant.account.getStringTypeAccount().equals(Account.BOSS)){
            GlobalConstant.menu = new DrawerBuilder()
                    .withActivity(this)
                    .withToolbar(toolbar)
                    .withAccountHeader(header)
                    .addDrawerItems(
                            item1,
                            new DividerDrawerItem(),
                            //item2,
                            item4,
                            item5,
                            item7,
                            item8,
                            //item12,
                            new DividerDrawerItem(),
                            item13
                    )
                    .withOnDrawerItemClickListener(this)
                    .withOnDrawerNavigationListener(this)
                    .withSelectedItem(R.integer.item_menu_home)
                    .build();
        }
        else
            if(GlobalConstant.account.getStringTypeAccount().equals(Account.CANDIDATE)){
                GlobalConstant.menu = new DrawerBuilder()
                        .withActivity(this)
                        .withToolbar(toolbar)
                        .withAccountHeader(header)
                        .addDrawerItems(
                                item1,
                                new DividerDrawerItem(),
                                //item2,
                                item3,
                                item4,
                                item5,
                                item6,
                                //item12,
                                new DividerDrawerItem(),
                                item13
                        )
                        .withOnDrawerItemClickListener(this)
                        .withOnDrawerNavigationListener(this)
                        .withSelectedItem(R.integer.item_menu_home)
                        .build();
            }



        Fragment fragment = new HomeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("mTitle",getResources().getString(R.string.item_menu_home));
        fragment.setArguments(bundle);
        Go.Replace(MainActivity.this,fragment);
    }


    private void GoProfile() {
        Fragment fragment = new ProfileFragment();
        String title = getResources().getString(R.string.item_menu_profile);
        Bundle bundle = new Bundle();
        bundle.putString("mTitle",title);
        bundle.putBoolean("mArrow",false);
        fragment.setArguments(bundle);
        Go.Replace(MainActivity.this,fragment);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        Object tag = drawerItem.getTag(); //id string
        if (tag != null) {
            int id = Integer.parseInt(tag.toString());
            String title = getResources().getString(id);
            //Toast.makeText(getBaseContext(), title, Toast.LENGTH_SHORT).show();

            Fragment fragment = null;
            switch (id) {
                case R.string.item_menu_home:
                    fragment = new HomeFragment();
                    break;
                case R.string.item_menu_notification:
                    fragment = new NotificationFragment();
                    break;
                case R.string.item_menu_find:
                    fragment = new FindFragment();
                    break;
                case R.string.item_menu_hot:
                    fragment = new HotFragment();
                    break;
                case R.string.item_menu_profile:
                    fragment = new ProfileFragment();
                    break;
                case R.string.item_menu_apply:
                    fragment = new ApplyFragment();
                    break;
                case R.string.item_menu_post:
                    fragment = new PostFragment();
                    break;
                case R.string.item_menu_candidate:
                    fragment = new CandidateFragment();
                    break;
                case R.string.item_menu_check:
                    fragment = new CheckFragment();
                    break;
                case R.string.item_menu_users:
                    fragment = new UserFragment();
                    break;
                case R.string.item_menu_report:
                    fragment = new ReportFragment();
                    break;
                case R.string.item_menu_setting:
                    fragment = new SettingFragment();
                    break;
                case R.string.item_menu_logout:
                    alert.show();
                    break;
            }
            if(fragment != null)
            {
                Bundle bundle = new Bundle();
                bundle.putString("mTitle",title);
                bundle.putBoolean("mArrow",false);
                fragment.setArguments(bundle);
                Go.Replace(MainActivity.this,fragment);
                return true;
            }
        }
        return false;
    }

    private void intDialogLogout() {

        alert = new MaterialDialog.Builder(this)
                .title(R.string.Logout)
                .titleColorRes(R.color.primary_color)
                .content(R.string.LogoutConfirm)
                .negativeText(R.string.cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                })
                .positiveColorRes(R.color.primary_color)
                .positiveText(R.string.logout)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        acceptLogout = true;
                        logout();
                    }
                }).build();
    }

    private void logout() {
        try {
            boolean b = SharedPreferencesUtils.setValue(this, USER_ACCOUNT, "");
            if (b) {
                Toast.makeText(this, getResources().getString(R.string.Logout_Successfully), Toast.LENGTH_SHORT).show();
                renewAll();
                onBackPressed();
            } else
                Toast.makeText(this, getResources().getString(R.string.Logout_Failure), Toast.LENGTH_SHORT).show();
        }catch (Exception e){}
    }

    @Override
    public void onBackPressed() {
        if(!acceptLogout && !getFragmentCurrent().isArrowMenu())
            alert.show();
        else {
            super.onBackPressed();
            GJobsFragment f = getFragmentCurrent();
            if (f != null && !acceptLogout) {
                f.UpdateToolbarTitle();
            }
        }
    }

    @Override
    public boolean onNavigationClickListener(View clickedView) {
        onBackPressed();
        return true;
    }

    public GJobsFragment getFragmentCurrent(){
        List<Fragment> list = this.getSupportFragmentManager().getFragments();
        int size = list.size();
        for(int i = size-1 ; i >=0;i--)
            if(list.get(i).isVisible())
                return (GJobsFragment) list.get(i);
        return null;
    }
}
