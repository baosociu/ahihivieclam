package hufi.gjobs.Views.Map;

import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.ButterKnife;
import hufi.gjobs.Master.GJobsActivity;
import hufi.gjobs.R;
import hufi.gjobs.Utils.Log;

public class MapsActivity extends GJobsActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    double _lat,_long;
    String _location;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_maps);
        this.setFinishOnTouchOutside(false);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getBundleExtra("BUNDLE");
         _lat = bundle.getDouble("LATITUDE",0);
        _long = bundle.getDouble("LONGITUDE",0);
        _location = bundle.getString("LOCATION","");

        MapFragment mapFragment = (MapFragment )getFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);

        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(_lat,_long);
        mMap.addMarker(new MarkerOptions().position(latLng).title(_location));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Log.i("Maps location: "+ latLng);
            }
        });
    }
}
