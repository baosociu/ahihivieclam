package hufi.gjobs.Master;

import android.content.Context;
import android.graphics.Bitmap;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;

import hufi.gjobs.Model.Account;
import hufi.gjobs.Model.Man;
import hufi.gjobs.R;
import hufi.gjobs.Utils.BitmapUtils;

/**
 * Created by baobao1996mn on 15/10/2017.
 */

public class GlobalConstant {
    public static final String USER_ACCOUNT= "USER_ACCOUNT";
    public static final int COUNT_MIN = 3;
    public static Drawer menu;
    public static AccountHeader header;
    public static ProfileDrawerItem profile;
    public static Account account; //todo: thông tin đăng nhập
    public static Man man;  //todo: thông tin chi tiết người dùng
    public static final int COUNT_PAGE = 10;
    //UPDATE MENU HEADER
    public static void updateHeaderMenu(Context context){
        if(GlobalConstant.man != null) {
                Bitmap avatar = BitmapUtils.getBitmapFromDrawable(context, R.drawable.gjobsjpg);
            if(man.getImage() != null && !man.getImage().isEmpty())
                avatar = BitmapUtils.getCircleBitmap(BitmapUtils.decodeBase64(man.getImage()));
                profile = new ProfileDrawerItem()
                        .withIdentifier(1234)
                        .withEmail(man.getEmail())
                        .withName(man.getNameMan())
                        .withIcon(avatar);
                header.removeProfile(0);
                header.addProfiles(profile);
        }
    }


    public static void renewAll(){
        menu = null;
        header= null;
        profile= null;
        account= null;
        man= null;
    }
}
