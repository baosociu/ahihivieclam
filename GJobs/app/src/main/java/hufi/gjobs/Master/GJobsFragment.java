package hufi.gjobs.Master;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import hufi.gjobs.Utils.ToolbarUtils;

/**
 * Created by baobao1996mn on 15/10/2017.
 */

public class GJobsFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    String mTitle;
    Boolean mArrow;
    public    GJobsActivity myActivity;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle = "";
        mArrow = false;
        if (getArguments() != null) {
            mTitle = getArguments().getString("mTitle");
            mArrow = getArguments().getBoolean("mArrow");
        }
        myActivity = ((GJobsActivity) this.getActivity());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        UpdateToolbarTitle();
    }

    public boolean isArrowMenu(){
        return mArrow;
    }

    public void UpdateToolbarTitle(){
        if(mArrow)
            ToolbarUtils.showBackArrow((AppCompatActivity) getContext(),GlobalConstant.menu,mTitle);
        else
            ToolbarUtils.showHamburgerIcon((AppCompatActivity) getContext(),GlobalConstant.menu,mTitle);
    }
}
