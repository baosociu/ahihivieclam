package hufi.gjobs.Master;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.github.ybq.android.spinkit.style.DoubleBounce;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import hufi.gjobs.R;

import static android.R.attr.translationZ;

/**
 * Created by baobao1996mn on 15/10/2017.
 */

public class GJobsActivity  extends AppCompatActivity {
    private Unbinder unbinder;
    ProgressBar loadingBar;
    RelativeLayout relativeLoading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        relativeLoading = new RelativeLayout(this);
        relativeLoading.setBackgroundColor(getResources().getColor(R.color.primary_color));
        relativeLoading.setAlpha((float)1.0);

        loadingBar = new ProgressBar(this);
        DoubleBounce doubleBounce = new DoubleBounce();
        doubleBounce.setColor(getResources().getColor(R.color.white));
        loadingBar.setIndeterminateDrawable(doubleBounce);
        relativeLoading.setVisibility(View.GONE);
    }

    public void setLoading(RelativeLayout relativeLayout){
        RelativeLayout.LayoutParams layoutParams =new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        relativeLoading.addView(loadingBar,layoutParams);

        RelativeLayout.LayoutParams layoutParams_ = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        relativeLayout.addView(relativeLoading,layoutParams_);
        ViewCompat.setTranslationZ(relativeLoading, translationZ);
    }

    public void showLoading(){
        relativeLoading.setVisibility(View.VISIBLE);
    }

    public void hideLoading(){
        relativeLoading.setVisibility(View.GONE);
    }


    @Override
    protected void onStart() {
        super.onStart();
        unbinder = ButterKnife.bind(this);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
