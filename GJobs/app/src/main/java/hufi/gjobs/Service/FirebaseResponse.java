package hufi.gjobs.Service;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import hufi.gjobs.R;

/**
 * Created by baobao1996mn on 22/10/2017.
 */

public class FirebaseResponse {
    final String COMPLETE_STATUS = "COMPLETE_STATUS";
    final String LOADING_STATUS = "LOADING_STATUS";
    final String FAILURE_STATUS = "FAIL_STATUS";
    final String SUCCESSFULLY_STATUS = "SUCCESSFULLY_STATUS";

    public static final String QUERY_ACTIVE = "ACTIVE";
    public static final String QUERY_CHANGPASS = "CHANGPASS";
    public static final String QUERY_SENDCODE = "SENDCODE";
    public static final String QUERY_LOGIN = "LOGIN";
    public static final String QUERY_FIND = "FIND";
    public static final String QUERY_LOGOUT = "LOGOUT";
    public static final String QUERY_REGISTER = "REGISTER";
    public static final String QUERY_UPDATE_MAN = "UPDATE_MAN";
    public static final String QUERY_ADD_POST = "ADD_POST";
    public static final String QUERY_CLOSETOPIC = "CLOSETOPIC";
    public static final String QUERY_STATUSPOST = "STATUSPOST";
    public static final String QUERY_APPLY_POST = "APPLY_POST";
    public static final String QUERY_PLUS_APPLY_POST = "PLUS_APPLY_POST";
    public static final String QUERY_REPORT_POST = "REPORT_POST";
    public static final String QUERY_REMOVE_REPORT_POST = "REMOVE_REPORT_POST";
    public static final String QUERY_STATUSUSER= "=STATUSUSER";


    String result;
    String message;
    String status;
    Object data;
    DataSnapshot snapshot;
    FirebaseDatabase database;
    DatabaseReference myRef;
    Context context;

    public FirebaseResponse(Context context) {
        status = COMPLETE_STATUS;
        result = SUCCESSFULLY_STATUS;
        data = null;
        message = "";
        this.context = context;
    }

    protected void Init(String table) {
        this.database = FirebaseDatabase.getInstance();
        this.myRef = this.database.getReference(table);        //root
        this.myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                snapshot = dataSnapshot;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setComplete() {
        this.status = COMPLETE_STATUS;
    }

    public void setLoading() {
        this.status = LOADING_STATUS;
    }

    public void setFailureResult(String message) {
        this.result = FAILURE_STATUS;
        this.message = message;
        this.setComplete();
    }

    public void setSuccessfullyResult(Object datareturn, String message) {
        this.result = SUCCESSFULLY_STATUS;
        this.data = datareturn;
        this.message = message;
        this.setComplete();
    }

    public String getStringSuccessfully(String query) {
        int id = R.string.Login_Successfully;
        switch (query) {
            case QUERY_ACTIVE:
                id = R.string.Active_Account_Successfully;
                break;
            case QUERY_CHANGPASS:
                id = R.string.PassChange_Successfully;
                break;
            case QUERY_SENDCODE:
                id = R.string.Send_Code_Successfully;
                break;
            case QUERY_FIND:
                id = R.string.Find_Successfully;
                break;
            case QUERY_LOGOUT:
                id = R.string.Logout_Successfully;
                break;
            case QUERY_REGISTER:
                id = R.string.Register_Successfully;
                break;
            case QUERY_UPDATE_MAN:
                id = R.string.UpdateMan_Successfully;
                break;
            case QUERY_ADD_POST:
                id = R.string.AddPost_Successfully;
                break;
            case QUERY_CLOSETOPIC:
                id = R.string.Closepost_Successfully;
                break;
            case QUERY_APPLY_POST:
                id = R.string.Apply_Successfully;
                break;
            case QUERY_STATUSPOST:
                id = R.string.ChangeStatusPost_Successfully;
                break;
            case QUERY_PLUS_APPLY_POST:
                id = R.string.PlusCountApplySuccessfully;
                break;
            case QUERY_REPORT_POST:
                id = R.string.ReportPostSuccessfully;
                break;
            case QUERY_REMOVE_REPORT_POST:
                id = R.string.RemovePostSuccessfully;
                break;
            case QUERY_STATUSUSER:
                id = R.string.ChangeUserStatusSuccessfully;
                break;
        }
        return context.getResources().getString(id);
    }

    public String getStringFailure(String query) {
        int id = R.string.Login_Failure;
        switch (query) {
            case QUERY_ACTIVE:
                id = R.string.Active_Account_Failure;
                break;
            case QUERY_CHANGPASS:
                id = R.string.PassChange_Failure;
                break;
            case QUERY_SENDCODE:
                id = R.string.Send_Code_Failure;
                break;
            case QUERY_FIND:
                id = R.string.Find_Failure;
                break;
            case QUERY_LOGOUT:
                id = R.string.Logout_Failure;
                break;
            case QUERY_REGISTER:
                id = R.string.Register_Failure;
                break;
            case QUERY_UPDATE_MAN:
                id = R.string.UpdateMan_Failure;
                break;
            case QUERY_ADD_POST:
                id = R.string.AddPost_Failure;
                break;
            case QUERY_CLOSETOPIC:
                id = R.string.Closepost_Failure;
                break;
            case QUERY_APPLY_POST:
                id = R.string.Apply_Failure;
                break;
            case QUERY_STATUSPOST:
                id = R.string.ChangeStatusPost_Failure;
                break;
            case QUERY_PLUS_APPLY_POST:
                id = R.string.PlusCountApplyFailure;
                break;
            case QUERY_REPORT_POST:
                id = R.string.ReportPostFailure;
                break;
            case QUERY_REMOVE_REPORT_POST:
                id = R.string.RemovePostFailure;
                break;
            case QUERY_STATUSUSER:
                id = R.string.ChangeUserStatusFailure;
                break;
        }
        return context.getResources().getString(id);
    }

    public String getMessageResult() {
        return message;
    }

    public boolean isComplete() {
        return status.equals(COMPLETE_STATUS);
    }

    public boolean isLoading() {
        return status.equals(LOADING_STATUS);
    }

    public boolean isSuccessfully() {
        return result.equals(SUCCESSFULLY_STATUS);
    }

    public boolean isFailure() {
        return result.equals(FAILURE_STATUS);
    }

    public Object getData() {
        return this.data;
    }

}
