package hufi.gjobs.Service;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import hufi.gjobs.Model.Report;

/**
 * Created by baobao1996mn on 09/11/2017.
 */

public class FirebaseReport extends FirebaseResponse  {
    final String type = "REPORT";
    public FirebaseReport(Context context) {
        super(context);
        Init(type);
    }

    public void findReportyByKey(final String idPost, final String idReportman) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    for (DataSnapshot dt : snapshot.getChildren()) {
                        Report report = dt.getValue(Report.class);
                        if (report.getIdPost().equals(idPost) && report.getIdReportman().equals(idReportman)) {
                            setSuccessfullyResult(report,getStringSuccessfully(QUERY_FIND));
                            break;
                        }
                    }
                    if (getData() == null)
                        setFailureResult(getStringFailure(QUERY_FIND));
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        setLoading();
        handler.post(runnable);
    }

    public void addReport(final Report report) {
        setLoading();
        String key = createKey(report.getIdPost(),report.getIdReportman());
        myRef.child(key).setValue(report).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    setSuccessfullyResult(report,getStringSuccessfully(QUERY_REPORT_POST));
                } else {
                    setFailureResult(getStringFailure(QUERY_REPORT_POST));
                }
            }
        });
    }

    private String createKey(String idPost, String idReportman){
        return idPost+idReportman;
    }

    public void removeReport(String idPost, String idReportman){
        String key = createKey(idPost,idReportman);
        setLoading();
        myRef.child(key).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful())
                    setSuccessfullyResult(null,getStringSuccessfully(QUERY_REMOVE_REPORT_POST));
                else
                    setFailureResult(QUERY_REMOVE_REPORT_POST);
            }
        });
    }

    public void getAllReport() {
        setLoading();
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(snapshot != null){
                    List<Report> reportList = new ArrayList<Report>();
                    for (DataSnapshot r : snapshot.getChildren())
                        reportList.add(r.getValue(Report.class));
                    setSuccessfullyResult(reportList,"");
                    handler.removeCallbacks(this);
                }
                else
                    handler.post(this);
            }
        });
    }
}
