package hufi.gjobs.Service;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import hufi.gjobs.Model.Account;
import hufi.gjobs.Model.Apply;
import hufi.gjobs.R;
import hufi.gjobs.Utils.DateUtils;
import hufi.gjobs.Utils.Log;

/**
 * Created by baobao1996mn on 22/10/2017.
 */

public class FirebaseAccount extends FirebaseResponse {


    private static final String Type = "ACCOUNT";

    public FirebaseAccount(Context context) {
        super(context);
        Init(Type);
    }

    public void login(final String username, final String password) {
        final String type_query = QUERY_LOGIN;
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    for (DataSnapshot dt : snapshot.getChildren()) {
                        Account ac = dt.getValue(Account.class);
                        if (ac.getUsername().equals(username) && ac.getPassword().equals(password)) {
                            if (ac.getStatus() == Account.STATUS_ACTIVED)
                                setSuccessfullyResult(ac, getStringSuccessfully(type_query));
                            else
                                setSuccessfullyResult(ac, context.getResources().getString(R.string.Register_Failure_Disactived));
                            break;
                        }
                    }
                    if (!isComplete())
                        setFailureResult(getStringFailure(type_query));
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        setLoading();
        handler.post(runnable);
    }

    public void addAccount(final Account account) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    //todo: check username
                    boolean check = true;
                    for (DataSnapshot dt : snapshot.getChildren())
                        if (dt.getValue(Account.class).getUsername().equals(account.getUsername())
                                || dt.getValue(Account.class).getEmail().equals(account.getEmail())) {
                            check = false;
                            break;
                        }
                    if (check) {
                        String key = getCountUserByNow(snapshot);
                        account.setIdAccount(key);
                        myRef.child(key).setValue(account).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    setSuccessfullyResult(account, getStringSuccessfully(QUERY_REGISTER));
                                } else {
                                    setFailureResult(getStringFailure(QUERY_REGISTER));
                                }
                            }
                        });
                    } else {
                        setFailureResult(context.getResources().getString(R.string.Register_Duplication));
                    }
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        setLoading();
        handler.post(runnable);

    }

    public void findAccountByUsernameAndEmail(final String username, final String email) {
        final String type_query = QUERY_FIND;
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    for (DataSnapshot dt : snapshot.getChildren()) {
                        Account a = (Account) dt.getValue(Account.class);
                        if (a.getEmail().equals(email) || a.getUsername().equals(username)) {
                            setSuccessfullyResult(a, getStringSuccessfully(type_query));
                            break;
                        }
                    }
                    if (getData() == null)
                        setFailureResult(getStringFailure(type_query));
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        setLoading();
        handler.post(runnable);
    }

    public void findAccountByUsernameAndEmail_Forgotpass(final String username, final String email) {
        final String type_query = QUERY_FIND;
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    for (DataSnapshot dt : snapshot.getChildren()) {
                        Account a = (Account) dt.getValue(Account.class);
                        if (a.getEmail().equals(email) && a.getUsername().equals(username)) {
                            setSuccessfullyResult(a, getStringSuccessfully(type_query));
                            break;
                        }
                    }
                    if (getData() == null)
                        setFailureResult(getStringFailure(type_query));
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        setLoading();
        handler.post(runnable);
    }

    public void update(final Account account, final String type_query) {
        final String key = account.getIdAccount();
        Log.i("Update: " + myRef.getKey() + "\\" + key);

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                myRef.child(key).setValue(account).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful())
                            setSuccessfullyResult(account, getStringSuccessfully(type_query));
                        else
                            setFailureResult(getStringFailure(type_query));
                    }
                });
            }
        };
        setLoading();
        handler.post(runnable);
    }


    private String getCountUserByNow(DataSnapshot datasnapshot) {
        int count = 0;
        for (DataSnapshot d : datasnapshot.getChildren()) {
            Account a = d.getValue(Account.class);
            String ma = a.getIdAccount().substring(0, 8);
            if (ma.equals(DateUtils.createKeyByNow()))
                count++;
            Log.i(ma);
        }
        String key = ("0000" + (count + 1)); //tạo key
        key.substring(key.length() - 4);
        key = DateUtils.createKeyByNow() + key;
        return key;
    }

    public void getAllAccount(final String searchKey) {
        setLoading();
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    List<Account> output = new ArrayList<Account>();
                    for (DataSnapshot d : snapshot.getChildren()) {
                        Account a = d.getValue(Account.class);
                        if (searchKey.isEmpty())
                            output.add(a);
                        else if (a.getUsername().contains(searchKey) || a.getEmail().contains(searchKey))
                            output.add(a);
                    }
                    setSuccessfullyResult(output, "");
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        });
    }

    public void updateAccount(final Account acc, final String typeQuery) {
        setLoading();
        myRef.child(acc.getIdAccount()).setValue(acc).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    setSuccessfullyResult(acc, getStringSuccessfully(typeQuery));
                } else {
                    setFailureResult(getStringFailure(typeQuery));
                }
            }
        });
    }

    public void getAppliedAccount(String idReciut,final String searchKey) {
        setLoading();
        final FirebaseApply firebaseApply = new FirebaseApply(context);
        firebaseApply.getAppliesByReciut(idReciut);

        final Handler handlerApply = new Handler();
        handlerApply.post(new Runnable() {
            @Override
            public void run() {
                if(firebaseApply.isComplete()){
                    if(firebaseApply.isSuccessfully())
                    {
                        final List<Apply> applies = (ArrayList<Apply>)firebaseApply.getData();

                        final Handler handler = new Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (snapshot != null) {
                                    List<Account> output = new ArrayList<Account>();
                                    for (Apply a : applies) {
                                        Account acc = ContainsApply(snapshot,a.getIdCandidate());
                                        if(acc != null) {
                                            if (searchKey.isEmpty())
                                                output.add(acc);
                                            else if (acc.getUsername().contains(searchKey) || acc.getEmail().contains(searchKey))
                                                output.add(acc);
                                        }
                                    }

                                    setSuccessfullyResult(new Object[]{output,applies}, "");
                                    handler.removeCallbacks(this);
                                } else
                                    handler.post(this);
                            }
                        });
                    }
                    handlerApply.removeCallbacks(this);
                }
                else
                    handlerApply.post(this);
            }
        });


    }

    private Account ContainsApply(DataSnapshot snapshot, String idCandidate) {
        for(DataSnapshot s : snapshot.getChildren())
        {
            Account a = s.getValue(Account.class);
            if(a.getIdAccount().equals(idCandidate))
                return a;
        }
        return null;
    }

    public void findAccountByEmail(final String email) {
        final String type_query = QUERY_FIND;
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    for (DataSnapshot dt : snapshot.getChildren()) {
                        Account a = (Account) dt.getValue(Account.class);
                        if (a.getEmail().equals(email)) {
                            setSuccessfullyResult(a, getStringSuccessfully(type_query));
                            break;
                        }
                    }
                    if (getData() == null)
                        setFailureResult(getStringFailure(type_query));
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        setLoading();
        handler.post(runnable);
    }

    public void findAccountByUser(final String user) {
        final String type_query = QUERY_FIND;
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    for (DataSnapshot dt : snapshot.getChildren()) {
                        Account a = (Account) dt.getValue(Account.class);
                        if (a.getUsername().equals(user)) {
                            setSuccessfullyResult(a, getStringSuccessfully(type_query));
                            break;
                        }
                    }
                    if (getData() == null)
                        setFailureResult(getStringFailure(type_query));
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        setLoading();
        handler.post(runnable);
    }

    public void findAccountByID(final String id) {
        final String type_query = QUERY_FIND;
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    for (DataSnapshot dt : snapshot.getChildren()) {
                        Account a = (Account) dt.getValue(Account.class);
                        if (a.getIdAccount().equals(id)) {
                            setSuccessfullyResult(a, getStringSuccessfully(type_query));
                            break;
                        }
                    }
                    if (getData() == null)
                        setFailureResult(getStringFailure(type_query));
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        setLoading();
        handler.post(runnable);
    }



    //login google
}
