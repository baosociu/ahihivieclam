package hufi.gjobs.Service;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;

import hufi.gjobs.Model.Man;
import hufi.gjobs.Utils.Log;

/**
 * Created by baobao1996mn on 22/10/2017.
 */

public class FirebaseMan extends FirebaseResponse {
    public static final String TYPE_BOSS = "BOSS"; //1
    public static final String TYPE_ADMIN = "ADMIN"; //2
    public static final String TYPE_CANDIDATE = "CANDIDATE "; //0
    String type;

    public FirebaseMan(Context context, int id_type) {
        super(context);
        this.type = TYPE_CANDIDATE;
        if (id_type == 1)
            this.type = TYPE_BOSS;
        else if (id_type == 2)
            this.type = TYPE_ADMIN;
        Init(type);
    }


    public void addMan(final Man man) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    String key = man.getIdMan();
                    myRef.child(key).setValue(man).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                setSuccessfullyResult(null,getStringSuccessfully(QUERY_REGISTER));
                            } else {
                                setFailureResult(getStringFailure(QUERY_REGISTER));
                            }
                        }
                    });
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        setLoading();
        handler.post(runnable);
    }

    public void updateMan(final Man man, final String typeQuery){
        String key = man.getIdMan();
        Log.i("Update: "+myRef.getKey()+"\\"+key);
        setLoading();
        myRef.child(key).setValue(man).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful())
                    setSuccessfullyResult(man, getStringSuccessfully(typeQuery));
                else
                    setFailureResult(getStringFailure(typeQuery));
            }
    });
    }

    public void findManByKey(final String key) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    for (DataSnapshot dt : snapshot.getChildren()) {
                        Man man = dt.getValue(Man.class);
                        if (man.getIdMan().equals(key)) {
                            setSuccessfullyResult(man,getStringSuccessfully(QUERY_FIND));
                            break;
                        }
                    }
                    if (getData() == null)
                        setFailureResult(getStringFailure(QUERY_FIND));
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        setLoading();
        handler.post(runnable);
    }


}
