package hufi.gjobs.Service;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import hufi.gjobs.R;

/**
 * Created by Duc on 26/10/2017.
 */

public class SendMailTask extends AsyncTask<Object,Object,Object> {

    private ProgressDialog statusDialog;
    private Context context;

    public SendMailTask(Context context) {
        this.context = context;
    }

    @Override
    protected Object doInBackground(Object... params) {
        try {

            publishProgress("Xử lý tài khoản....");
            GMailSender androidEmail = new GMailSender(params[0].toString(),
                    params[1].toString());

            publishProgress("Đang chuẩn bị gửi thư....");
            androidEmail.sendMail(params[2].toString(), params[3].toString(), params[0].toString(),
                    params[4].toString());
            publishProgress("SendMailTask");
        } catch (Exception e) {
            publishProgress(e.getMessage());

        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        statusDialog = new ProgressDialog(context);
        statusDialog.setMessage("Sẵn sàng...");
        statusDialog.setIndeterminate(false);
        statusDialog.setCancelable(false);
        statusDialog.show();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        Toast.makeText(context,context.getResources().getString(R.string.Mail_Complete),Toast.LENGTH_SHORT).show();
        statusDialog.dismiss();
    }

    @Override
    protected void onProgressUpdate(Object... values) {
        super.onProgressUpdate(values);
        statusDialog.setMessage(values[0].toString());
    }
}
