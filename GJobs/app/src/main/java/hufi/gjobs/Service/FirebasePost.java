package hufi.gjobs.Service;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import hufi.gjobs.Model.Apply;
import hufi.gjobs.Model.Post;
import hufi.gjobs.Model.Report;
import hufi.gjobs.Utils.Log;

/**
 * Created by baobao1996mn on 08/11/2017.
 */

public class FirebasePost extends FirebaseResponse {
    public static final String type = "POST";


    public FirebasePost(Context context) {
        super(context);
        Init(type);
    }

    public void getReportPost(final String title) {
        setLoading();

        final FirebaseReport firebaseReport = new FirebaseReport(context);
        firebaseReport.getAllReport();

        final Handler handlerReport = new Handler();
        handlerReport.post(new Runnable() {
            @Override
            public void run() {
                if (firebaseReport.isComplete()) {
                    if (firebaseReport.isSuccessfully()) {
                        final List<Report> reportList = (ArrayList<Report>) firebaseReport.getData();
                        Log.i("SIZE REPORT: " + reportList.size());
                        final Handler handler = new Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (snapshot != null) {
                                    List<Post> output = new ArrayList<Post>();
                                    for (Report r : reportList) {
                                        Post p = ContainsPost(snapshot, r.getIdPost());
                                        if (p != null) {
                                            if (title.isEmpty())
                                                output.add(p);
                                            else if (p.getTitlePost().contains(title))
                                                output.add(p);
                                        }
                                    }
                                    setSuccessfullyResult(new Object[]{output, reportList}, "");
                                    handler.removeCallbacks(this);
                                    handlerReport.removeCallbacks(this);
                                } else
                                    handler.post(this);
                            }
                        });
                    }
                } else
                    handlerReport.post(this);
            }
        });
    }

    private Post ContainsPost(DataSnapshot snapshot, String idPost) {
        for (DataSnapshot s : snapshot.getChildren()) {
            Post p = s.getValue(Post.class);
            if (p.getIdPost().equals(idPost))
                return p;
        }
        return null;
    }

    public void getHotPost(final String title) {
        setLoading();

        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    List<Post> output = new ArrayList<Post>();

                    for (DataSnapshot d : snapshot.getChildren()) {
                        Post p = d.getValue(Post.class);
                        if (p.getStatusPost() == Post.STATUS_POST_APPROVED) {
                            if (title.isEmpty()) {
                                output.add(p);
                            } else if (p.getTitlePost().contains(title)) {
                                output.add(p);
                            }
                        }
                    }

                    //todo sắp xếp nè
                    for (int i = 0; i < output.size() - 1; i++) {
                        for (int j = i + 1; j < output.size(); j++) {
                            if (output.get(i).getCountApply() < output.get(j).getCountApply()) {
                                Post q = output.get(i);
                                output.set(i, output.get(j));
                                output.set(j, q);
                            }
                        }
                    }

                    setSuccessfullyResult(output, "");
                    handler.removeCallbacks(this);
                }
                else
                    handler.post(this);
            }
        });
    }


    public void getApplyPost(final String idCandidate, final String title) {
        setLoading();

        final FirebaseApply firebaseApply = new FirebaseApply(context);
        firebaseApply.getAppliesByCandidate(idCandidate);

        final Handler handlerApply = new Handler();
        handlerApply.post(new Runnable() {
            @Override
            public void run() {
                if (firebaseApply.isComplete()) {
                    if (firebaseApply.isSuccessfully()) {
                        final List<Apply> applyList = (ArrayList<Apply>) firebaseApply.getData();
                        Log.i("SIZE APPLY: " + applyList.size());
                        final Handler handler = new Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (snapshot != null) {
                                    List<Post> output = new ArrayList<Post>();
                                    for (DataSnapshot d : snapshot.getChildren()) {
                                        Post p = d.getValue(Post.class);
                                        if (ContainsApply(applyList, p.getIdPost())) {
                                            if (title.isEmpty())
                                                output.add(p);
                                            else if (p.getTitlePost().contains(title))
                                                output.add(p);
                                        }
                                    }
                                    setSuccessfullyResult(output, "");
                                    handler.removeCallbacks(this);
                                    handlerApply.removeCallbacks(this);
                                } else
                                    handler.post(this);
                            }
                        });
                    }
                } else
                    handlerApply.post(this);
            }
        });
    }

    private boolean ContainsApply(List<Apply> applyList, String idPost) {
        for (Apply a : applyList)
            if (a.getIdPost().equals(idPost))
                return true;
        return false;
    }

    public void getApprovedPost(final String title) {
        setLoading();
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    List<Post> output = new ArrayList<Post>();
                    for (DataSnapshot d : snapshot.getChildren()) {
                        Post p = d.getValue(Post.class);
                        //todo neesiu post được kiểm duyệt
                        if (p.getStatusPost() == Post.STATUS_POST_APPROVED) {
                            if (title.isEmpty())
                                output.add(p);
                            else if (p.getTitlePost().contains(title))
                                output.add(p);
                        }
                    }
                    setSuccessfullyResult(output, "");
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        });
    }

    public void getAllPost(final String title) {
        setLoading();
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    List<Post> output = new ArrayList<Post>();
                    for (DataSnapshot d : snapshot.getChildren()) {
                        Post p = d.getValue(Post.class);
                        //todo neesiu post được kiểm duyệt
                        if (title.isEmpty())
                            output.add(p);
                        else if (p.getTitlePost().contains(title))
                            output.add(p);
                    }
                    setSuccessfullyResult(output, "");
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        });
    }


    public void getAllPostByIDReciut(final String idReciut, final String title) {
        setLoading();
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    List<Post> output = new ArrayList<Post>();
                    for (DataSnapshot d : snapshot.getChildren()) {
                        Post p = d.getValue(Post.class);
                        Log.i("INPUT: IDRECIUT: " + idReciut + " - TITLE: " + title);
                        Log.i("OUTPUT IDRECIUT: " + p.getIdReciut() + " - TITLE: " + p.getTitlePost());
                        if (p.getIdReciut().equals(idReciut)) //search
                        {

                            if (title.isEmpty())
                                output.add(p);
                            else if (p.getTitlePost().contains(title))
                                output.add(p);
                        }

                    }
                    setSuccessfullyResult(output, "");
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        });
    }

    public void updatePost(final Post post, final String typeQuery) {
        setLoading();
        myRef.child(post.getIdPost()).setValue(post).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    setSuccessfullyResult(post, getStringSuccessfully(typeQuery));
                } else {
                    setFailureResult(getStringFailure(typeQuery));
                }
            }
        });
    }

    public void updatePost(final String idPost, final String typeQuery) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    boolean isFind = false;
                    for (DataSnapshot dt : snapshot.getChildren()) {
                        Post post = dt.getValue(Post.class);
                        if (post.getIdPost().equals(idPost)) {
                            //todo update post
                            final Post postUpdated = changeInforPost(post, typeQuery);
                            myRef.child(postUpdated.getIdPost()).setValue(postUpdated).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        setSuccessfullyResult(postUpdated, getStringSuccessfully(typeQuery));
                                    } else {
                                        setFailureResult(getStringFailure(typeQuery));
                                    }
                                }
                            });
                            isFind = true;
                            break;
                        }
                    }
                    if (!isFind)
                        setFailureResult(getStringFailure(QUERY_FIND));
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        setLoading();
        handler.post(runnable);
    }

    private Post changeInforPost(Post post, String typeQuery) {
        switch (typeQuery) {
            case QUERY_CLOSETOPIC:
                post.setStatusPost(Post.STATUS_POST_CLOSED);
                break;
        }
        return post;
    }

    public void addPost(final Post post) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    String key = "POST" + snapshot.getChildrenCount();
                    post.setIdPost(key);
                    myRef.child(key).setValue(post).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                setSuccessfullyResult(post, getStringSuccessfully(QUERY_ADD_POST));
                            } else {
                                setFailureResult(getStringFailure(QUERY_ADD_POST));
                            }
                        }
                    });
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        setLoading();
        handler.post(runnable);
    }


    public void findPostByID(final String key) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    for (DataSnapshot dt : snapshot.getChildren()) {
                        Post post = dt.getValue(Post.class);
                        if (post.getIdPost().equals(key)) {
                            setSuccessfullyResult(post, getStringSuccessfully(QUERY_FIND));
                            break;
                        }
                    }
                    if (getData() == null)
                        setFailureResult(getStringFailure(QUERY_FIND));
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        setLoading();
        handler.post(runnable);
    }
}
