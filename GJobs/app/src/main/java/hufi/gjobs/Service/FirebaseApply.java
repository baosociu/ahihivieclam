package hufi.gjobs.Service;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import hufi.gjobs.Model.Apply;

/**
 * Created by baobao1996mn on 08/11/2017.
 */

public class FirebaseApply extends FirebaseResponse {
    final String type = "APPLY";
    public FirebaseApply(Context context) {
        super(context);
        Init(type);
    }

    public void findApplyByKey(final String idCandidate, final String idPost) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (snapshot != null) {
                    for (DataSnapshot dt : snapshot.getChildren()) {
                        Apply apply = dt.getValue(Apply.class);
                        if (apply.getIdPost().equals(idPost) && apply.getIdCandidate().equals(idCandidate)) {
                            setSuccessfullyResult(apply,getStringSuccessfully(QUERY_FIND));
                            break;
                        }
                    }
                    if (getData() == null)
                        setFailureResult(getStringFailure(QUERY_FIND));
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        setLoading();
        handler.post(runnable);
    }

    public void addApply(final Apply apply) {
        setLoading();
        myRef.child(apply.getIdPost()+apply.getIdCandidate()).setValue(apply).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    setSuccessfullyResult(apply,getStringSuccessfully(QUERY_APPLY_POST));
                } else {
                    setFailureResult(getStringFailure(QUERY_APPLY_POST));
                }
            }
        });
    }

    public void getAppliesByCandidate(final String idCandidate) {
        setLoading();
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(snapshot != null){
                    List<Apply> data = new ArrayList<Apply>();
                    for(DataSnapshot d : snapshot.getChildren())
                    {
                        Apply apply = d.getValue(Apply.class);
                        if(apply.getIdCandidate().equals(idCandidate))
                            data.add(apply);
                    }
                    setSuccessfullyResult(data,"");
                    handler.removeCallbacks(this);
                }
                else
                    handler.post(this);
            }
        });
    }

    public void getAllApply() {
        setLoading();
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(snapshot != null){
                    List<Apply> data = new ArrayList<Apply>();
                    for(DataSnapshot d : snapshot.getChildren())
                    {
                        Apply apply = d.getValue(Apply.class);
                        data.add(apply);
                    }
                    setSuccessfullyResult(data,"");
                    handler.removeCallbacks(this);
                }
                else
                    handler.post(this);
            }
        });
    }

    public void getAppliesByReciut(final String idReciut) {
        setLoading();
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(snapshot != null){
                    List<Apply> data = new ArrayList<Apply>();
                    for(DataSnapshot d : snapshot.getChildren())
                    {
                        Apply apply = d.getValue(Apply.class);
                        if(apply.getIdReciut().equals(idReciut))
                            data.add(apply);
                    }
                    setSuccessfullyResult(data,"");
                    handler.removeCallbacks(this);
                }
                else
                    handler.post(this);
            }
        });
    }
}
