package hufi.gjobs.Utils;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

/**
 * Created by baobao1996mn on 17/09/2017.
 */

public class DrawableUtils {
    public static Drawable getIconAwesome(Context context, FontAwesome.Icon icon, int color) {
        Drawable drawable = new IconicsDrawable(context)
                .icon(icon)
                .color(color);
        //drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        return drawable;
    }

    public static Drawable getIconAwesomeActionBar(Context context, FontAwesome.Icon icon, int color) {
        Drawable drawable = new IconicsDrawable(context)
                .icon(icon)
                .color(color)
                .actionBar();
        //drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        return drawable;
    }
}
