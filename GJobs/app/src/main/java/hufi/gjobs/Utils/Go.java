package hufi.gjobs.Utils;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import hufi.gjobs.Master.GlobalConstant;
import hufi.gjobs.R;


/**
 * Created by baobao1996mn on 24/09/2017.
 */

public class Go {

    //TODO: ko add vào stack
    static public void Replace(AppCompatActivity activity, Fragment fragment) {
        android.support.v4.app.FragmentManager manager = activity.getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.container, fragment).commit();
        manager.executePendingTransactions();
        if(GlobalConstant.menu.isDrawerOpen())
            GlobalConstant.menu.closeDrawer();
    }

    //TODO: add vào stack
    static public void Add(AppCompatActivity activity, Fragment fragment, int IdTag) {
        String tag = activity.getResources().getString(IdTag);
        android.support.v4.app.FragmentManager manager = activity.getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.container, fragment).addToBackStack(tag).commit();
        manager.executePendingTransactions();
        if (GlobalConstant.menu.isDrawerOpen())
            GlobalConstant.menu.closeDrawer();
    }

}
