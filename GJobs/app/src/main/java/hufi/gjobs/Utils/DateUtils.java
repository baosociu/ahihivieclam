package hufi.gjobs.Utils;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Created by baobao1996mn on 22/10/2017.
 */

public class DateUtils {
    public static Date getDate(int day, int month, int year){
        Calendar c = Calendar.getInstance();
        c.set(year,month,day);
        return c.getTime();
    }

    public static String createKeyByNow(){
        Calendar c= Calendar.getInstance();
        String str = Utils.dateToString(c.getTime(),"dd/MM/yyyy");
        return str.replace("/","");
    }

    private void fakePosts(){
        for (int i = 1 ; i <= 20 ; i++) {
            Random r = new Random();

            String id = i < 10 ? "0"+i : i+"";
            id ="2017102200"+id;

            String id_province = r.nextInt(63)+"";
            if(id_province.length() == 1)
                id_province = "0" +id_province;

            String id_ntd = (1+r.nextInt(99))+"";
            if(id_ntd.length() == 1)
                id_ntd = "0" + id_ntd;

            String id_topic = "TP00";
            id_topic = id_topic + r.nextInt(10);

//            Post post = new Post(id, id_topic,"TINH0"+id_province, "NTD00000"+id_ntd, "Tuyển dụng Nhân viên", "Nội dung tuyển dụng nhân viên",
//                    DateUtils.getDate(r.nextInt(27)+1, r.nextInt(11)+1, 2017), DateUtils.getDate(r.nextInt(27)+1, r.nextInt(11)+1, 2018), DateUtils.getDate(22,10, 2017), 1);
            DatabaseReference _myRef = FirebaseDatabase.getInstance().getReference();
            //_myRef.child("Post").child(id).setValue(post);
        }
    }
}
