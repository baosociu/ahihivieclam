package hufi.gjobs.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by VietHoa on 12/01/16.
 */
public class Utils {
    private static final String TAG = "Utils";

    /**
     * Android Support TextView
     *
     * Make link clickable inside TextView
     */



    public static String stringToDateString(String input, String inputFormat, String outFormat) {
        SimpleDateFormat format = new SimpleDateFormat(inputFormat);
        String result = "";
        try {
            Date date = format.parse(input);

            format = new SimpleDateFormat(outFormat);

            result = format.format(date);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static Date stringToDate(String input, String inputFormat) {
        SimpleDateFormat format = new SimpleDateFormat(inputFormat);
        try {
            Date result = new Date();
            result = format.parse(input);
            return result;

        } catch (Exception e) {
        }

        return null;
    }

    public static String dateToString(Date input, String outFormat) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(outFormat);
            String result = format.format(input);
            return result;
        }catch (Exception e){


        }
        return  null;
    }

    public static String dateStringNow(String outFormat) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(outFormat);
            String result = format.format(Calendar.getInstance().getTime());
            return result;
        }catch (Exception e){


        }
        return  null;
    }
}
