package hufi.gjobs.Utils;

import android.support.v7.app.AppCompatActivity;

import com.mikepenz.materialdrawer.Drawer;

/**
 * Created by baobao1996mn on 19/09/2017.
 */

public class ToolbarUtils {
    public static void showBackArrow(AppCompatActivity context, Drawer drawer,int idTitle) {
        context.getSupportActionBar().setTitle(idTitle);
        drawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(false);
        context.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        isHamburgerIcon = false;
    }
    public static void showBackArrow(AppCompatActivity context, Drawer drawer,String title) {
        context.getSupportActionBar().setTitle(title);
        drawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(false);
        context.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        isHamburgerIcon = false;
    }
    public static void showHamburgerIcon(AppCompatActivity context, Drawer drawer, int idTitle) {
        context.getSupportActionBar().setTitle(idTitle);
        context.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        drawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
        isHamburgerIcon = true;
    }
    public static void showHamburgerIcon(AppCompatActivity context, Drawer drawer, String title) {
        context.getSupportActionBar().setTitle(title);
        context.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        drawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
        isHamburgerIcon = true;

    }
    static  public  boolean isHamburgerIcon;
}
