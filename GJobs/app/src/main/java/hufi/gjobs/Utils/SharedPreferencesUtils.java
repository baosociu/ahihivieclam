package hufi.gjobs.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.lang.reflect.Type;

import hufi.gjobs.R;

/**
 * Created by baobao1996mn on 24/10/2017.
 */

public class SharedPreferencesUtils {
    public static Object getValue(Context context, String key, Type type){
        try{
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.app_name),Context.MODE_PRIVATE);
            String str = sharedPreferences.getString(key,"");
            Gson g = new Gson();
            return g.fromJson(str,type);
        }catch (Exception e){
            Log.i("getValue: "+e.getMessage());
        }
        return null;
    }

    public static boolean setValue(Context context, String key, Object value){
        try{
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.app_name),Context.MODE_PRIVATE);
            Gson g = new Gson();
            String str = g.toJson(value);
            sharedPreferences.edit().putString(key,str).apply();
            return true;
        }catch (Exception e){
            Log.i("getValue: "+e.getMessage());
        }
        return false;
    }
}
