package hufi.gjobs.Model;

/**
 * Created by baobao1996mn on 22/10/2017.
 */

public class Account {
    public static final int TYPE_CANDIDATE = 0;
    public static final int TYPE_BOSS = 1;
    public static final int TYPE_ADMIN = 2;

    public static final String CANDIDATE = "Người tìm việc";
    public static final String ADMIN = "Admin";
    public static final String BOSS = "Nhà tuyển dụng";

    public static final int STATUS_DISACTIVED = 0;
    public static final int STATUS_ACTIVED = 1;
    public static final int STATUS_BLOCKED = 2;

    String idAccount;
    String username;
    String password;
    int typeAccount; //todo: 0 ==> người tìm việc Candidate, 1 ==> người tuyển dụng Boss, 2 ==> admin
    int status;     //todo: 0 ==> chưa kích hoạt, 1 ==> đã kích hoạt, -1 ==> bị khoá
    boolean online; //đang login ?
    String email;
    String activeCode;

    public Account(){}

    public Account(String username, String password, int typeAccount, String email) {
        this.username = username;
        this.password = password;
        this.typeAccount = typeAccount;
        this.status = STATUS_DISACTIVED;
        this.online = false;
        this.email = email;
    }
    
    public Account(Account base){
        this.idAccount = base.getIdAccount();
        this.username = base.getUsername();
        this.password = base.getPassword();
        this.typeAccount = base.getTypeAccount();
        this.status = base.getStatus();
        this.online = base.isOnline();
        this.email = base.getEmail();
        this.activeCode = base.getActiveCode();
    }

    public  String getStringTypeAccount(){
        switch (typeAccount){
            case TYPE_CANDIDATE:
                return CANDIDATE;
            case TYPE_BOSS:
                return BOSS;
            case TYPE_ADMIN:
                return ADMIN;
        }
        return "";
    }

    public boolean isCandidate() {
        return typeAccount == TYPE_CANDIDATE;
    }
    public boolean isBoss() {
        return typeAccount == TYPE_BOSS;
    }
    public boolean isAdmin() {
        return typeAccount == TYPE_ADMIN;
    }

    public String getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(String idAccount) {
        this.idAccount = idAccount;
    }

    public String getActiveCode() {
        return activeCode;
    }

    public void setActiveCode(String activeCode) {
        this.activeCode = activeCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTypeAccount() {
        return typeAccount;
    }

    public void setTypeAccount(int typeAccount) {
        this.typeAccount = typeAccount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }
}
