package hufi.gjobs.Model;

import java.util.Date;

/**
 * Created by baobao1996mn on 08/11/2017.
 */

public class Apply {
    String idCandidate;
    String idPost;
    Date dateApply;
    String Reciut;

    public Apply(){}

    public Apply(String idCandidate, String Reciut, String idPost, Date dateApply) {
        this.idCandidate = idCandidate;
        this.idPost = idPost;
        this.dateApply = dateApply;
        this.Reciut = Reciut;
    }

    public String getIdReciut() {
        return Reciut;
    }

    public void setIdReciut(String idRecuit) {
        this.Reciut = idRecuit;
    }

    public String getIdCandidate() {
        return idCandidate;
    }

    public void setIdCandidate(String idCandidate) {
        this.idCandidate = idCandidate;
    }

    public String getIdPost() {
        return idPost;
    }

    public void setIdPost(String idPost) {
        this.idPost = idPost;
    }

    public Date getDateApply() {
        return dateApply;
    }

    public void setDateApply(Date dateApply) {
        this.dateApply = dateApply;
    }
}
