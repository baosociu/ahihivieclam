package hufi.gjobs.Model;

import java.util.Date;

/**
 * Created by baobao1996mn on 22/10/2017.
 */
//người tìm việc
public class Man {
    String idMan;
    String nameMan;
    String address;
    int idProvince;
    String numberphone;
    String email;
    String image;
    boolean gender;
    Date dob;

    public Man() {
    }

    public Man(String idMan, String nameMan, String address, int idProvince, String numberphone, String email, String image, Date dob, boolean gender) {
        this.idMan = idMan;
        this.nameMan = nameMan;
        this.address = address;
        this.idProvince = idProvince;
        this.numberphone = numberphone;
        this.email = email;
        this.image = image;
        this.dob = dob;
        this.gender = gender;
    }
    
    public Man(Man base){
        this.idMan = base.idMan;
        this.nameMan = base.nameMan;
        this.address = base.address;
        this.idProvince = base.idProvince;
        this.numberphone = base.numberphone;
        this.email = base.email;
        this.image = base.image;
        this.dob = base.dob;
        this.gender = base.gender;
    }

    public Date getDob() {
        return dob;
    }

    public boolean getGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getIdMan() {
        return idMan;
    }

    public void setIdMan(String idMan) {
        this.idMan = idMan;
    }

    public String getNameMan() {
        return nameMan;
    }

    public void setNameMan(String nameMan) {
        this.nameMan = nameMan;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getIdProvince() {
        return idProvince;
    }

    public void setIdProvince(int idProvince) {
        this.idProvince = idProvince;
    }

    public String getNumberphone() {
        return numberphone;
    }

    public void setNumberphone(String numberphone) {
        this.numberphone = numberphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
