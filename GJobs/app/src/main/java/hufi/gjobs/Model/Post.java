package hufi.gjobs.Model;

import java.util.Date;

/**
 * Created by baobao1996mn on 17/10/2017.
 */

public class Post {
    public static final Integer STATUS_POST_WAITING = 0;
    public static final Integer STATUS_POST_APPROVED = 1;
    public static final Integer STATUS_POST_BLOCKED = 2;
    public static final Integer STATUS_POST_CLOSED = 3; //do nhà tuyển dụng quyết định

    String idPost;      //MATINTUYENDUNG
    int idTopic;     //MACHUDE ==> ngành
    int idProvince;  //MATINH
    String idReciut;    //MANHATUYENDUNG
    String titlePost;   //TIEUDE
    String contentPost; //NOIDUNG
    String address;
    Date dateStart;     //NGÀYBATDAU tuyển dụng
    Date dateEnd;       //NGÀYKETTHUC tuyển dụng
    Date datePost;      //NgàyĐăng
    int statusPost;     //0: đang chờ - 1: được duyệt  - 2: Khoá bài
    int countApply;

    public Post(){}

    public Post(int idTopic, int idProvince, String idReciut,
                String titlePost, String contentPost, Date dateStart,
                Date dateEnd, Date datePost, String address) {
        this.idPost = "";
        this.idProvince = idProvince;
        this.idReciut = idReciut;
        this.titlePost = titlePost;
        this.contentPost = contentPost;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.datePost = datePost;
        this.statusPost = STATUS_POST_WAITING;
        this.idTopic = idTopic;
        this.countApply = 0;
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCountApply() {
        return countApply;
    }

    public void setCountApply(int countApply) {
        this.countApply = countApply;
    }

    public int getIdTopic() {
        return idTopic;
    }

    public void setIdTopic(int idTopic) {
        this.idTopic = idTopic;
    }

    public String getIdPost() {
        return idPost;
    }

    public void setIdPost(String idPost) {
        this.idPost = idPost;
    }

    public int getIdProvince() {
        return idProvince;
    }

    public void setIdProvince(int idProvince) {
        this.idProvince = idProvince;
    }

    public String getIdReciut() {
        return idReciut;
    }

    public void setIdReciut(String idReciut) {
        this.idReciut = idReciut;
    }

    public String getTitlePost() {
        return titlePost;
    }

    public void setTitlePost(String titlePost) {
        this.titlePost = titlePost;
    }

    public String getContentPost() {
        return contentPost;
    }

    public void setContentPost(String contentPost) {
        this.contentPost = contentPost;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Date getDatePost() {
        return datePost;
    }

    public void setDatePost(Date datePost) {
        this.datePost = datePost;
    }

    public int getStatusPost() {
        return statusPost;
    }

    public void setStatusPost(int statusPost) {
        this.statusPost = statusPost;
    }
}
