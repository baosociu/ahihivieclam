package hufi.gjobs.Model;

import java.util.Date;

/**
 * Created by baobao1996mn on 09/11/2017.
 */

public class Report {
    String idPost;
    String idReportman;
    Date dateReport;
    String reason;
    int idReason;

    public Report(){

    }

    public Report(String idPost, String idReportman, Date dateReport, String reason, int idReason) {
        this.idPost = idPost;
        this.idReportman = idReportman;
        this.dateReport = dateReport;
        this.reason = reason;
        this.idReason = idReason;
    }

    public String getIdPost() {
        return idPost;
    }

    public void setIdPost(String idPost) {
        this.idPost = idPost;
    }

    public String getIdReportman() {
        return idReportman;
    }

    public void setIdReportman(String idReportman) {
        this.idReportman = idReportman;
    }

    public Date getDateReport() {
        return dateReport;
    }

    public void setDateReport(Date dateReport) {
        this.dateReport = dateReport;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getIdReason() {
        return idReason;
    }

    public void setIdReason(int idReason) {
        this.idReason = idReason;
    }
}
